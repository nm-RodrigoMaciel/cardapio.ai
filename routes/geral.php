<?php

use App\Controller;
use App\Http\Response;

$obRouter->get('', [
    function () {
        readfile('https://cardapio.ai/inicio/index.php', 'r');
    }
]);

$obRouter->get('/whitelabel', [
    function ($request, $data) {
        return new Response(200, Controller\Inicio::getInicio($request, $data));
    }
]);

//Lojas

$obRouter->get('lojas', [
    function () {
        return new Response(200, Controller\Lojas::getLojas());
    }
]);

//Produto

$obRouter->post('getProductByCod', [
    function () {
        return new Response(200, Controller\Produto::getProductByCod());
    }
]);

$obRouter->post('updateQtdProduto', [
    function () {
        return new Response(200, Controller\Produto::updateQtdProduto());
    }
]);

$obRouter->post('updateQtdAdicionais', [
    function () {
        return new Response(200, Controller\Produto::updateQtdAdicionais());
    }
]);

$obRouter->post('updateQtdAdicionaisRadio', [
    function () {
        return new Response(200, Controller\Produto::updateQtdAdicionaisRadio());
    }
]);

$obRouter->post('updateQtdIngredientes', [
    function () {
        return new Response(200, Controller\Produto::updateQtdIngredientes());
    }
]);

$obRouter->post('getQuantityObject', [
    function () {
        return new Response(200, Controller\Produto::getQuantityObject());
    }
]);

$obRouter->post('setProductsObject', [
    function () {
        return new Response(200, Controller\Produto::setObject());
    }
]);

$obRouter->post('getProductsObject', [
    function () {
        return new Response(200, Controller\Produto::getObject());
    }
]);

//Lojas

$obRouter->post('getStores', [
    function () {
        return new Response(200, Controller\Lojas::getLojasByLocation());
    }
]);

$obRouter->post('indicacao', [
    function () {
        return new Response(200, Controller\Lojas::indicacao());
    }
]);


$obRouter->post('latLongAddress', [
    function () {
        return new Response(200, Controller\Online\Carrinho::latLongAddress());
    }
]);

$obRouter->post('simulateShipping', [
    function () {
        return new Response(200, Controller\Inicio::simulateShipping());
    }
]);

$obRouter->post('seeLog', [
    function () {
        return new Response(200, Controller\Produto::registerErro());
    }
]);

$obRouter->post('plan', [
    function () {
        return new Response(200, Controller\Inicio::planVerify());
    }
]);