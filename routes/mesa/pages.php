<?php

use App\Controller\Mesa;
use App\Http\Response;

$obRouter->post('getCart', [
    function () {
        return new Response(200, Mesa\Carrinho::getCart());
    }
]);

$obRouter->post('updateObject', [
    function () {
        return new Response(200, Mesa\Carrinho::updateObject());
    }
]);

$obRouter->post('verifyFinishCart', [
    function () {
        return new Response(200, Mesa\Carrinho::verifyFinishCart());
    }
]);

$obRouter->post('finishCart', [
    function () {
        return new Response(200, Mesa\Carrinho::finishCart());
    }
]);

$obRouter->post('finalBill', [
    function () {
        return new Response(200, Mesa\Carrinho::finalBill());
    }
]);

$obRouter->post('setToken', [
    function () {
        return new Response(200, Mesa\Carrinho::setToken());
    }
]);

// $obRouter->post('verifySales', [
//     function () {
//         return new Response(200, Mesa\Carrinho::verifySales());
//     }
// ]);