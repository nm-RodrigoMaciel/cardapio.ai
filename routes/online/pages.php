<?php

use App\Controller\Online;
use App\Http\Response;

$obRouter->post('getCart', [
    function () {
        return new Response(200, Online\Carrinho::getCart());
    }
]);

$obRouter->post('updateObject', [
    function () {
        return new Response(200, Online\Carrinho::updateObject());
    }
]);

$obRouter->post('setAddress', [
    function () {
        return new Response(200, Online\Carrinho::setAddress());
    }
]);

$obRouter->post('setPaymentMethod', [
    function () {
        return new Response(200, Online\Carrinho::setPaymentMethod());
    }
]);

$obRouter->post('getPaymentMethod', [
    function () {
        return new Response(200, Online\Carrinho::getPaymentMethod());
    }
]);

$obRouter->post('setChange', [
    function () {
        return new Response(200, Online\Carrinho::setChange());
    }
]);

$obRouter->post('updateChange', [
    function () {
        return new Response(200, Online\Carrinho::updateChange());
    }
]);

$obRouter->post('finalBill', [
    function () {
        return new Response(200, Online\Conta::finalBill());
    }
]);

$obRouter->post('verifyCoupon', [
    function () {
        return new Response(200, Online\Carrinho::verifyCoupon());
    }
]);

$obRouter->post('setSchedule', [
    function () {
        return new Response(200, Online\Carrinho::setSchedule());
    }
]);

$obRouter->post('cleanCoupom', [
    function () {
        return new Response(200, Online\Carrinho::cleanCoupom());
    }
]);

$obRouter->post('setToken', [
    function () {
        return new Response(200, Online\Carrinho::setToken());
    }
]);

$obRouter->post('openArea', [
    function () {
        return new Response(200, Online\Carrinho::openArea());
    }
]);

$obRouter->post('setWithdraw', [
    function () {
        return new Response(200, Online\Carrinho::setWithdraw());
    }
]);

// $obRouter->post('verifySales', [
//     function () {
//         return new Response(200, Online\Carrinho::verifySales());
//     }
// ]);