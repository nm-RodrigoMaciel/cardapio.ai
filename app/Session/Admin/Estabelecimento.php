<?php

namespace App\Session\Admin;

class Estabelecimento
{

    private static function init()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    public static function estabelecimento($data)
    {
        self::init();

        $_SESSION['cardapio_user'] = [
            'nomeEstabelecimento' => $data['nomeEstabelecimento'],
            'codigoEstabelecimento' => $data['codigoEstabelecimento'],
            'tipoEstabelecimento' => $data['tipoEstabelecimento'],
            'path' => $data['path'],
            'api' => $data['api'],
            'logo' => $data['logo'],
            'colorPrimary' => $data['colorPrimary'],
            'phone' => $data['phone'],
            'ordenacaoProdutos' => $data['ordenacaoProdutos']
        ];
    }

    public static function getUser()
    {
        self::init();

        return $_SESSION['cardapio_user'] ?? '';
    }

    public static function getData($var)
    {
        self::init();

        return $_SESSION['cardapio_user'][$var] ?? '';
    }

    public static function setData($var, $val)
    {
        self::init();

        return $_SESSION['cardapio_user'][$var] = $val;
    }

    public static function userCod()
    {
        self::init();

        return self::getData('nomeEstabelecimento') . self::getData('codigoEstabelecimento');
    }
}
