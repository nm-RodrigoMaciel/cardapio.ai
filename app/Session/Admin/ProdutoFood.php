<?php

namespace App\Session\Admin;

use App\Model\Address;

class ProdutoFood
{

    private static function init()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    public static function setProduto($produto)
    {
        self::init();

        unset($_SESSION['produtoSelecionado']);
        $_SESSION['produtoSelecionado']['produto'][0] = $produto;
        $_SESSION['produtoSelecionado']['quantidade'] = 1;
        $_SESSION['produtoSelecionado']['valorFinal'] = $_SESSION['produtoSelecionado']['produto'][0]['valorInicial'];

        if (
            isset($_SESSION['produtoSelecionado']['produto'][0]['categorias']) &&
            sizeof($_SESSION['produtoSelecionado']['produto'][0]['categorias']) > 0
        ) {
            foreach ($_SESSION['produtoSelecionado']['produto'][0]['categorias'] as $key => $values) {
                if (!$values['obrigatorio'] && $values['quantidade'] > 0)
                    $_SESSION['produtoSelecionado']['utils']['fixo'][$values['id']] = $values['quantidade'];
            }
        }
    }

    public static function updateProduto($data)
    {
        self::init();

        if (isset($data['produto'])) {

            $_SESSION['produtoSelecionado']['quantidade'] += $data['quantidade'];

            //Não permitir que a quantidade de produtos seja negativa
            if ($_SESSION['produtoSelecionado']['quantidade'] <= 1) {
                $_SESSION['produtoSelecionado']['quantidade'] = 1;
                $response = [
                    'qtd' => 1,
                    'plus' => true,
                    'less' => false
                ];
            } else {
                $response = [
                    'qtd' => $_SESSION['produtoSelecionado']['quantidade'],
                    'plus' => true,
                    'less' => true
                ];
            }

            $response['final'] = self::calcProduto();

            return json_encode($response);
        } else if (isset($data['ingrediente'])) {

            $key = $data['ingrediente'];

            if (isset($_SESSION['produtoSelecionado']['produto'][0]['itensAdicionais'][$key])) {

                $quantidade = $_SESSION['produtoSelecionado']['produto'][0]['itensAdicionais'][$key]['quantidade'];

                //Caso esse ingrediente não tenha sido manipulado ainda, adicionar ele na lista de ingredientesEscolhidos
                if (!isset($_SESSION['produtoSelecionado']['ingredientesEscolhidos'][$key])) {
                    $_SESSION['produtoSelecionado']['ingredientesEscolhidos'][$key] = $_SESSION['produtoSelecionado']['produto'][0]['itensAdicionais'][$key];
                    $_SESSION['produtoSelecionado']['ingredientesEscolhidos'][$key]['quantidadeAlterada'] = 0;
                }

                //Alterar quantidade do ingrediente dentro da lista de ingredientesEscolhidos
                $_SESSION['produtoSelecionado']['ingredientesEscolhidos'][$key]['quantidadeAlterada'] += $data['quantidade'];

                $response = [
                    'qtd' => $quantidade - $_SESSION['produtoSelecionado']['ingredientesEscolhidos'][$key]['quantidadeAlterada'],
                    'plus' => true,
                    'less' => true
                ];

                //Caso o ingrediente volte pra quantidade original, removelo da lista ingredientesEscolhidos
                if ($response['qtd'] >= $quantidade) {
                    unset($_SESSION['produtoSelecionado']['ingredientesEscolhidos'][$key]);

                    $response = [
                        'qtd' => $quantidade,
                        'plus' => false,
                        'less' => true
                    ];
                }

                //Não permitir que a quantidade de ingredientes seja negativa
                if ($response['qtd'] <= 0) {
                    $_SESSION['produtoSelecionado']['ingredientesEscolhidos'][$key]['quantidadeAlterada'] = $quantidade;
                    $response = [
                        'qtd' => 0,
                        'plus' => true,
                        'less' => false
                    ];
                }

                return json_encode($response);
            }
        } else if (isset($data['adicionais'])) {

            //Caso tenha chegado no limite da categoria, interromper
            function limiteCategoria($categoriaAdicional, $data)
            {
                if (isset($_SESSION['produtoSelecionado']['utils']['fixo'][$categoriaAdicional])) {
                    return ($_SESSION['produtoSelecionado']['utils']['variavel'][$categoriaAdicional] + $data['quantidade']) <= $_SESSION['produtoSelecionado']['utils']['fixo'][$categoriaAdicional];
                } else
                    return true;
            }

            //Caso tenha chegado no limite do adicional, interromper
            function limiteAdicional($data)
            {
                $key = $data['adicionais'];
                if ($_SESSION['produtoSelecionado']['produto'][0]['itensAdicionais'][$key]['quantidadeEscolha'] > 0) {
                    return ($_SESSION['produtoSelecionado']['adicionais'][$key]['quantidade'] + $data['quantidade']) <= $_SESSION['produtoSelecionado']['produto'][0]['itensAdicionais'][$key]['quantidadeEscolha'];
                } else
                    return true;
            }

            //Verificar se a quantidade é igual a zero
            function igualZero($data)
            {
                $key = $data['adicionais'];
                return $_SESSION['produtoSelecionado']['adicionais'][$key]['quantidade'] <= 0;
            }

            $key = $data['adicionais'];

            if (isset($_SESSION['produtoSelecionado']['produto'][0]['itensAdicionais'][$key])) {

                $adicionalExisteExterno = isset($_SESSION['produtoSelecionado']['adicionais'][$key]);
                if (!$adicionalExisteExterno && $data['quantidade'] < 0) {
                    $response = ['qtd' => 0, 'plus' => true, 'less' => false];
                } else {

                    //Verifica se o adicional existe na lista externa
                    if (!$adicionalExisteExterno) {
                        $_SESSION['produtoSelecionado']['adicionais'][$key] = $_SESSION['produtoSelecionado']['produto'][0]['itensAdicionais'][$key];
                        $_SESSION['produtoSelecionado']['adicionais'][$key]['quantidade'] = 0;
                    }

                    $categoriaAdicional = $_SESSION['produtoSelecionado']['produto'][0]['itensAdicionais'][$key]['categoria'];
                    if ($data['quantidade'] > 0) {

                        if (limiteCategoria($categoriaAdicional, $data) && limiteAdicional($data)) {
                            $_SESSION['produtoSelecionado']['adicionais'][$key]['quantidade'] += $data['quantidade'];
                            $_SESSION['produtoSelecionado']['utils']['variavel'][$categoriaAdicional] += $data['quantidade'];
                        }

                        $response = ['qtd' => $_SESSION['produtoSelecionado']['adicionais'][$key]['quantidade']];
                    } else {
                        $_SESSION['produtoSelecionado']['adicionais'][$key]['quantidade'] += $data['quantidade'];
                        $_SESSION['produtoSelecionado']['utils']['variavel'][$categoriaAdicional] += $data['quantidade'];
                        $response = ['qtd' => $_SESSION['produtoSelecionado']['adicionais'][$key]['quantidade']];

                        if ($_SESSION['produtoSelecionado']['adicionais'][$key]['quantidade'] <= 0) {
                            unset($_SESSION['produtoSelecionado']['adicionais'][$key]);
                            $response = ['qtd' => 0];
                        }
                    }

                    $response['all'] = limiteCategoria($categoriaAdicional, $data);
                    $response['plus'] = limiteAdicional($data);
                    $response['less'] = !igualZero($data);
                }

                $response['final'] = self::calcProduto();

                return json_encode($response);
            }
        } else if (isset($data['adicionalRadio'])) {

            $keyAdicional = $data['adicionalRadio'];
            $adicional = $_SESSION['produtoSelecionado']['produto'][0]['itensAdicionais'][$keyAdicional];
            $categoria = $adicional['categoria'];

            foreach ($_SESSION['produtoSelecionado']['adicionais'] as $key => $value) {
                if ($value['categoria'] == $categoria)
                    unset($_SESSION['produtoSelecionado']['adicionais'][$key]);
            }

            $_SESSION['produtoSelecionado']['adicionais'][$keyAdicional] = $adicional;
            $_SESSION['produtoSelecionado']['adicionais'][$keyAdicional]['quantidade'] = 0;
        }
    }

    public static function calcProduto()
    {
        $valorFinal = $_SESSION['produtoSelecionado']['produto'][0]['valorInicial'];
        foreach ($_SESSION['produtoSelecionado']['adicionais'] as $key => $values) {
            $valorFinal += ($values['valorInicial'] * $values['quantidade']);
        }
        $valorFinal *= $_SESSION['produtoSelecionado']['quantidade'];
        $_SESSION['produtoSelecionado']['valorFinal'] = $valorFinal;

        return number_format($valorFinal, 2, ',', '.');;
    }

    public static function getProduto($var)
    {
        self::init();

        return $_SESSION['produtoSelecionado'][$var];
    }

    public static function setProdutoFood($data)
    {
        self::init();
        $produtoSelecionado = $_SESSION['produtoSelecionado'];

        $object = $_SESSION[Estabelecimento::userCod()];

        if ($data['nota'] != null && $data['nota'] != '')
            $produtoSelecionado['notasAdicionais'] = $data['nota'];

        unset($produtoSelecionado['utils']);
        $object['carrinhoList'][time()] = $produtoSelecionado;

        unset($_SESSION['produtoSelecionado']);

        $_SESSION[Estabelecimento::userCod()] = $object;
    }

    public static function getProdutoFood()
    {
        self::init();

        return $_SESSION[Estabelecimento::userCod()] ?? null;
    }

    public static function getData($var)
    {
        self::init();

        return $_SESSION[Estabelecimento::userCod()][$var] ?? '';
    }

    public static function getTotalValue($frete = true)
    {
        self::init();

        $object = $_SESSION[Estabelecimento::userCod()];
        $total = 0;
        foreach ($object['carrinhoList'] as $key => $value) {
            $total += $value['valorFinal'];
        }

        if ($frete) {
            $frete = Address::getFreteValue();

            return $total + $frete;
        } else
            return $total;
    }

    public static function updateProdutoFood($index, $qtd, $frete = 0)
    {
        self::init();

        $object = $_SESSION[Estabelecimento::userCod()];
        $quantity = $object['carrinhoList'][$index]['quantidade'];

        $valueFinal = 0;
        if ($quantity > 0) {
            $currentValue = $object['carrinhoList'][$index]['valorFinal'] / $quantity;

            if ($qtd == 1) {
                $quantity += 1;
                $object['carrinhoList'][$index]['quantidade'] = $quantity;
            } else if ($qtd == 0) {
                $quantity -= 1;
                $object['carrinhoList'][$index]['quantidade'] = $quantity;
            }

            $valueFinal = $currentValue * $quantity;
            $object['carrinhoList'][$index]['valorFinal'] = $valueFinal;
        }

        if ($quantity < 1)
            unset($object['carrinhoList'][$index]);

        $_SESSION[Estabelecimento::userCod()] = $object;

        $total = 0;
        foreach ($object['carrinhoList'] as $key => $value) {
            $total += $value['valorFinal'];
        }

        return json_encode(array(
            'quantity' => $quantity,
            'value' => number_format($valueFinal, 2, ',', '.'),
            'subtotal' => number_format($total, 2, ',', '.'),
            'total' => number_format($total + $frete, 2, ',', '.')
        ));
    }

    public static function clearProdutoFood()
    {
        self::init();

        unset($_SESSION[Estabelecimento::userCod()]);
    }
}
