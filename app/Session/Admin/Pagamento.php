<?php

namespace App\Session\Admin;

use App\Session\Session;

class Pagamento extends Session
{
    public static $name = 'pagamento';

    //Variaveis
    public static $bandeiraCartao;
    public static $metodoPagamento;
    public static $tipoPagamento;
    public static $valorCompra;
    public static $valorDesconto;
    public static $valorFrete;
    public static $valorTotal;
    public static $valorPagoDinheiro;
    public static $troco;
    public static $cupomCompra;
    public static $cupomDesconto;
    public static $cupomObjeto;

    public static function setObject($data)
    {
        parent::set(self::$name, $data);
    }

    public static function setData($data)
    {
        parent::update(self::$name, $data);
    }

    public static function getData($var)
    {
        return parent::get(self::$name, $var);
    }

    public static function getObject()
    {
        return parent::object(self::$name);
    }

    public static function clearObject()
    {
        parent::clear(self::$name);
    }

    public static function removeData($data)
    {
        parent::remove(self::$name, $data);
    }

    public static function setPaymentMethod($data)
    {
        self::clearObject();
        self::setData($data);
    }

    public static function setCoupon($coupon)
    {
        self::init();

        if (!$coupon) {
            if (isset($_SESSION[self::$name]['cupomCompra']))
                unset($_SESSION[self::$name]['cupomCompra']);
        } else
            $_SESSION[self::$name]['cupomCompra'] = $coupon;
    }

    public static function removePaymentMethod()
    {
        self::init();

        unset($_SESSION[self::$name]);
    }
}
