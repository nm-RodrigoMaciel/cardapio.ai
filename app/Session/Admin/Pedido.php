<?php

namespace App\Session\Admin;

use App\Session\Session;

class Pedido extends Session
{
    public static $name = 'pedido';

    //Variaveis
    public static $dataProgramada;
    public static $retirada;
    public static $mesa;

    public static function setObject($data)
    {
        parent::set(self::$name, $data);
    }

    public static function setData($data)
    {
        parent::update(self::$name, $data);
    }

    public static function getData($var)
    {
        return parent::get(self::$name, $var);
    }

    public static function getObject()
    {
        return parent::object(self::$name);
    }

    public static function clearObject()
    {
        parent::clear(self::$name);
    }

    public static function removeData($vars)
    {
        parent::remove(self::$name, $vars);
    }

    public static function setSchedule($data)
    {
        self::setData([
            "dataProgramada" => $data['dataPrevisaoEntrega'],
            "dataProgramadaTexto" => $data['textoPrevisaoEntrega']
        ]);
    }
}
