<?php

namespace App\Session\Admin;

use App\Session\Session;

class Cliente extends Session
{
    public static $name = 'cliente';

    //Variaveis
    public static $nomeUsuario;
    public static $telefone;
    public static $cep;
    public static $bairro;
    public static $cidade;
    public static $estado;
    public static $complemento;
    public static $numero;
    public static $pais;
    public static $rua;
    public static $tokenNotificacao;

    public static function setObject($data)
    {
        parent::set(self::$name, $data);
    }

    public static function setData($data)
    {
        parent::update(self::$name, $data);
    }

    public static function getData($var)
    {
        return parent::get(self::$name, $var);
    }

    public static function getObject()
    {
        return parent::object(self::$name);
    }

    public static function clearObject()
    {
        parent::clear(self::$name);
    }

    public static function removeData($data)
    {
        parent::remove(self::$name, $data);
    }

    public static function clearAddress()
    {
        self::init();

        self::setData([
            "cep" => null,
            "bairro" => null,
            "cidade" => null,
            "estado" => null,
            "complemento" => null,
            "numero" => null,
            "pais" => null,
            "rua" => null,
        ]);
    }
}
