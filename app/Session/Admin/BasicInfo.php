<?php

namespace App\Session\Admin;

use App\Session\Session;

class BasicInfo extends Session
{
    public static $name = 'basicInfo';

    public static function setObject($data)
    {
        parent::set(self::$name, $data);
    }

    public static function setData($data)
    {
        parent::update(self::$name, $data);
    }

    public static function getData($var)
    {
        return parent::get(self::$name, $var);
    }

    public static function getObject()
    {
        return parent::object(self::$name);
    }

    public static function clearObject()
    {
        parent::clear(self::$name);
    }
    
    public static function removeData($data)
    {
        parent::remove(self::$name, $data);
    }
}