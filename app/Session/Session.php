<?php

namespace App\Session;

class Session
{
    protected static function init()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    protected static function set($type, $data)
    {
        self::init();

        self::clear($type);
        self::update($type, $data);
    }

    protected static function update($type, $data)
    {
        self::init();

        foreach ($data as $key => $value) {
            $_SESSION[$type][$key] = $value;
        }
    }

    protected static function get($type, $var)
    {
        self::init();

        return $_SESSION[$type][$var] ?? null;
    }

    protected static function object($type)
    {
        self::init();

        return $_SESSION[$type] ?? null;
    }

    protected static function remove($type, $vars)
    {
        self::init();

        foreach ($vars as $var) {
            unset($_SESSION[$type][$var]);
        }
    }

    protected static function clear($type)
    {
        self::init();

        unset($_SESSION[$type]);
    }
}
