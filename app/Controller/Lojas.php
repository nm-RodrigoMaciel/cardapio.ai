<?php

namespace App\Controller;

use App\Controller\Page;
use App\Model\Lojas as ModelLojas;
use App\Session\Admin\Cliente;
use App\Utils\View;

class Lojas extends Page
{
    public static function getLojas()
    {
        $view = 'lojas';

        $address = Cliente::getObject();

        $content = View::render($view, [
            'cep' => $address['cep'] ?? '',
            'rua' => $address['rua'] ?? '',
            'numero' => $address['numero'] ?? '',
            'bairro' => $address['bairro'] ?? '',
            'cidade' => $address['cidade'] ?? '',
            'estado' => $address['estado'] ?? '',
            'complemento' => $address['complemento'] ?? '',
            'disabled' => $address != null ? '' : 'disabled'
        ]);

        return parent::getPageOnly($view, $content);
    }

    public static function getLojasByLocation()
    {
        $view = 'utils/lojas/stores';
        $address = json_decode($_POST['cliente'], true);

        Cliente::setData([
            "cep" => $address['cep'],
            "bairro" => $address['bairro'],
            "cidade" => $address['cidade'],
            "estado" => $address['estado'],
            "complemento" => $address['complemento'],
            "numero" => $address['numero'],
            "pais" => "Brasil",
            "rua" => $address['rua']
        ]);

        $estabelecimentos = ModelLojas::listarParceiros($address);
        $tipos = [];
        $stores = '';
        $categories = '';
        if (sizeof($estabelecimentos) > 0) {
            foreach ($estabelecimentos as $key => $value) {

                $smallName = parent::removerAcentos($value['nome']);
                $smallName = preg_replace('/[^A-Za-z0-9\- ]/', '', $smallName);
                $smallName = preg_replace("/\s+/", "-", $smallName);
                $smallName = strtolower($smallName);

                $stores .= View::render('utils/lojas/store', [
                    'small-name' => $smallName,
                    'nome' => $value['nome'],
                    'tipo' => $value['tipoEstabelecimento'],
                    'url' => $value['url'],
                    'avaliacao' => number_format($value['avaliacaoTotal'], 1, ',', '.'),
                    'closed' => !$value['aberto']['status'] ? 'closed' : '',
                    'distancia' => parent::price($value['distancia']),
                    'tempo' => $value['tempoEntrega'],
                    'valor' => parent::price($value['valorFrete']),
                ]);

                $tipos[$value['tipoEstabelecimento']] = '';
            }

            foreach ($tipos as $key => $value) {
                $categories .= View::render('utils/lojas/categorie', [
                    'nome' => $key,
                    'nomeFix' => parent::lowerSpace($key)
                ]);
            }
        }

        return View::render($view, [
            'address' => $address['rua'] . ', ' . $address['numero'] . ', ' . $address['bairro'] . ', ' . $address['cidade'] . ', ' . $address['estado'],
            'stores' => $stores,
            'categories' => $categories,
            'empty' => $stores == '' ? 'empty' : ''
        ]);
    }

    public static function indicacao()
    {
        return ModelLojas::indicacao($_POST, Cliente::getObject());
    }
}
