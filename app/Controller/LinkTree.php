<?php

namespace App\Controller;

use App\Controller\Page;
use App\Session\Admin\BasicInfo;
use App\Session\Admin\Estabelecimento;
use App\Utils\View;

class LinkTree extends Page
{
    public static function getLinkTree($isRoot, $isMesa)
    {
        $view = 'linktree';

        $basicInfo = BasicInfo::getObject();

        $buttons = '';
        if ($basicInfo['botaoPedirOnline'])
            $buttons .= View::render('utils/linktree/pedir-online', [
                'path' => Estabelecimento::getData('path')
            ]);

        if ($basicInfo['botaoPedirNaMesa'])
            $buttons .= View::render('utils/linktree/pedir-na-mesa', [
                'path' => Estabelecimento::getData('path')
            ]);

        if (isset($basicInfo['linkTree'])) {
            foreach ($basicInfo['linkTree'] as $key => $values) {
                $buttons .= '<a class="linktree-button" href="' . $values['link'] . '" target="_blank">' . $values['title'] . '</a>';
            }
        }

        $timesItem = '';
        foreach ($basicInfo['horarios'] as $key => $value) {
            if ($value != null) {
                $timesItem .=
                    '<div class="linktree-profile-time-maximized-row">
                    <p>' . $value['titulo'] . '</p>
                    <p>' . $value['abre'] . ' às ' . $value['fecha'] . '</p>
                </div>';
            }
        }

        //Area de calcular frete
        $simulateShipping = '';
        if (!$isMesa)
            $simulateShipping = View::render('utils/linktree/simulate-shipping');

        $content = View::render($view, [
            'timesItem' => $timesItem,
            'nomeEstabelecimento' => Estabelecimento::getData('nomeEstabelecimento'),
            'path' => Estabelecimento::getData('path'),
            'endereco' => $basicInfo['endereco'],
            'aberto' => $basicInfo['aberto']['status'] ? '' : 'closed',
            'buttons' => $buttons,
            'pedido_minimo' => $basicInfo['valorMinimo'] > 0 ?
                View::render('utils/linktree/linktree-profile-payment', ['minimo' => parent::price($basicInfo['valorMinimo'])]) :
                View::render('utils/linktree/linktree-profile-nopayment'),
            'tipoEstabelecimento' => str_replace('ç', 'c', preg_replace("/\s+/", "", strtolower(Estabelecimento::getData('tipoEstabelecimento')))),
            'closed' => !$isRoot ? 'closed' : '',
            'simulateShipping' => $simulateShipping
        ]);

        return parent::getPageOnly($view, $content);
    }
}
