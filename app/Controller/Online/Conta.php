<?php

namespace App\Controller\Online;

use App\Controller\Page;
use App\Model\Cart;
use App\Session\Admin\BasicInfo;
use App\Session\Admin\Cliente;
use App\Utils\View;

class Conta extends Page
{
    public static function finalBill()
    {
        error_reporting(0);

        $bill =
            '<div class="no-bill bill-searched center-abs">
            <img class="center-rel-hor" src="/cardapio.ai/img/no-bill.svg">
            <svg width="10" height="9" viewBox="0 0 10 9" fill="none" xmlns="http://www.w3.org/2000/svg"
                style="position: absolute; left: 9.24em; top: 1.4px; z-index: -1;">
                <path
                    d="M4.86809 8.87164C7.32407 8.87164 9.31503 6.88566 9.31503 4.43582C9.31503 1.98599 7.32407 0 4.86809 0C2.41211 0 0.421143 1.98599 0.421143 4.43582C0.421143 6.88566 2.41211 8.87164 4.86809 8.87164Z"
                    fill="#E4002B" />
            </svg>
            <svg width="46" height="27" viewBox="0 0 46 27" fill="none" xmlns="http://www.w3.org/2000/svg"
                style="position: absolute; left: 114px; top: 2px;">
                <path
                    d="M4.86633 26.2502C4.43779 26.2499 4.02061 26.1126 3.67602 25.8585C3.33142 25.6043 3.07755 25.2467 2.95163 24.8381L0.0877185 15.507C0.0107597 15.2563 -0.0159464 14.993 0.00912889 14.732C0.0342041 14.4711 0.110567 14.2176 0.233858 13.9861C0.357148 13.7546 0.524955 13.5496 0.727685 13.3828C0.930416 13.216 1.1641 13.0907 1.4154 13.014L40.5349 1.06747C41.0424 0.912996 41.5906 0.96568 42.0592 1.21395C42.5277 1.46222 42.8784 1.8858 43.0343 2.39172L45.8982 11.723C46.0529 12.2292 46.0001 12.776 45.7512 13.2433C45.5023 13.7107 45.0777 14.0605 44.5706 14.2161L5.45097 26.1626C5.26153 26.2206 5.06449 26.2501 4.86633 26.2502V26.2502Z"
                    fill="#E4002B" />
            </svg>
            <svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"
                style="position: absolute; left: 13.1em; z-index: -1; top: 18.2px;">
                <path
                    d="M5.00528 9.66364C7.46125 9.66364 9.45221 7.67766 9.45221 5.22782C9.45221 2.77798 7.46125 0.791992 5.00528 0.791992C2.54931 0.791992 0.55835 2.77798 0.55835 5.22782C0.55835 7.67766 2.54931 9.66364 5.00528 9.66364Z"
                    fill="#E4002B" />
            </svg>
            <svg width="46" height="15" viewBox="0 0 46 15" fill="none" xmlns="http://www.w3.org/2000/svg"
                style="position: absolute; left: 165.5px; top: 26px;">
                <path
                    d="M43.4618 14.7491H2.54995C2.0194 14.7485 1.51075 14.538 1.1356 14.1638C0.760443 13.7896 0.549425 13.2822 0.548828 12.753V2.99417C0.549426 2.46495 0.760457 1.95757 1.13561 1.58336C1.51076 1.20914 2.0194 0.998643 2.54995 0.998047H43.4618C43.9924 0.998643 44.501 1.20914 44.8761 1.58336C45.2513 1.95757 45.4623 2.46495 45.4629 2.99417V12.753C45.4623 13.2822 45.2513 13.7896 44.8762 14.1638C44.501 14.538 43.9924 14.7485 43.4618 14.7491V14.7491Z"
                    fill="#E4002B" />
            </svg>
            <div>
                <span class="first">Sem pedidos no momento...</span></br>
                <span class="second">Assim que fizer algum pedido ele aparecerá aqui. :)</span>
            </div>
        </div>';

        $opened = Cart::verifyOpenCheckout(Cliente::getData('telefone'));

        $itens[1] = '';
        $itens[2] = '';
        if ($opened['statusCode'] == 200 && $opened['items'] != null) {

            $compras = $opened['items'];
            $bill =
                '<div class="bill center-rel-hor bill-searched">
                    <div class="title">
                        <span>Pedidos</span>
                    </div>';

            $countItem = 1;
            foreach ($compras as $key => $itens) {

                $valueFinal = 0;
                $pos = 1;
                $item_minimized = '';
                $retirada = $itens['pedido']['retirada'];

                foreach ($itens['carrinhoList'] as $indexCl => $valueCl) {

                    if ($valueCl != null) {

                        $value = $valueCl['produto'][0];

                        $item =
                            "<div class='item-pedido' ref='" . $indexCl . "'>
                                <div class='pedido-produto'>
                                    <img class='product-image' src='" . parent::https($value['url']) . "' onerror=this.src='/cardapio.ai/img/product-no-photo.svg'>
                                    <div class='description'>
                                        <span class='product-title'>" . $value['nomeProduto'] . "</span>
                                        <div class='buttons'>
                                            <div class='buttons-elegant elegant' ref='" . $indexCl . "' max='' min='0'>
                                                <div> 
                                                </div>
                                                <div> 
                                                    <span id='quantity-value'>" . $valueCl['quantidade'] . "x</span> 
                                                </div> 
                                                <div> 
                                                </div>
                                            </div> 
                                            <div class='value-area' ref='" . $indexCl . "'> 
                                                <span>R$</span> 
                                                <span>" . self::price($valueCl['valorFinal']) . "</span> 
                                            </div> 
                                        </div> 
                                    </div>
                                </div>";

                        $item_minimized .=
                            '<div class="item-minimized">
                                <p>' . $valueCl['quantidade'] . '</p>
                                <p>' . $value['nomeProduto'] . '</p>
                            </div>';

                        if (isset($valueCl['ingredientesEscolhidos'])) {
                            $item .= "<div class='pedido-extra'> <span class='title'>Ingredientes removidos</span>";
                            foreach ($valueCl['ingredientesEscolhidos'] as $indexIng => $valueIng) {
                                if (isset($valueIng['quantidadeAlterada']))
                                    $item .= " <span>- " . $valueIng['quantidadeAlterada'] . " " . $valueIng['nomeProduto'] . "</span> ";
                            }
                            $item .= "</div>";
                        }

                        $additional = '';
                        $uniqueChoice = '';
                        if (isset($valueCl['adicionais'])) {
                            foreach ($valueCl['adicionais'] as $indexAdi => $valueAdi) {
                                if ($valueAdi['quantidade'] == "0")
                                    $uniqueChoice .=
                                        " <span>• " . $valueAdi['nomeProduto'] . "</span> ";
                                else
                                    $additional .=
                                        " <span>+ " . $valueAdi['quantidade'] . "x " . $valueAdi['nomeProduto'] . "</span> ";
                            }
                        }

                        if ($additional != '') {
                            $item .=
                                "<div class='pedido-extra'> <span class='title'>Adicionais</span>" . $additional . "</div>";
                        }

                        if ($uniqueChoice != '') {
                            $item .=
                                "<div class='pedido-extra'> <span class='title'>Seção obrigatória</span>" . $uniqueChoice . "</div>";
                        }

                        if (isset($valueCl['notasAdicionais'])) {
                            $item .=
                                "<div class='pedido-extra observacao'> <span class='title'>Observação</span> <span>" . $valueCl['notasAdicionais'] . "</span> </div>";
                        }

                        $item .=
                            "</div>";

                        $itens[$pos] .= $item;

                        if ($pos == 1)
                            $pos = 2;
                        else
                            $pos = 1;

                        $valueFinal += $valueCl['valorCompra'] * $valueCl['quantidade'];
                    }
                }

                $status = end($itens['pedido']['status'])['descricao'];
                $statusText = $status;
                if ($status == "Transporte" && $retirada)
                    $statusText = "Pronto p/ retirada";

                $bill .=
                    '<div class="subtitle">
                                <div>
                                    <p>Pedido ' . $itens['utils']['codigoPedido'] . '</p>
                                    <p>' . $itens['utils']['dataPedido'] . '</p>
                                </div>
                                <div class="subtitle-status ' . ($status == "Finalizado" ? "noblink" : ($status == "Cancelada" ? "noblink error" : "")) . '">
                                    <img src="/cardapio.ai/img/point-green.svg">
                                    <span>' . $statusText . '</span>
                                </div>
                            </div> ' .
                    ($countItem > 1 ?
                        '<div class="minimized-item">   
                                    <div class="itens-minimized">
                                        ' . $item_minimized . '
                                    </div> 
                                    <div class="expandable">
                                        <p class="expandable-title">Total • R$ ' . self::price($itens['pagamento']['valorFrete'] + $itens['pagamento']['valorCompra']) . '</p>
                                        <div class="expandable-buttom-open">
                                            <p>Ver detalhes</p>
                                            <svg width="15" height="8" viewBox="0 0 15 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13.6591 0.000122054L15 1.21349L7.5 8.00012L8.09298e-08 1.21349L1.34091 0.000121907L7.5 5.57339L13.6591 0.000122054Z" fill="#E4002B"/>
                                            </svg>
                                        </div>
                                    </div> 
                                    <div class="divider"></div>
                                </div>'
                        : '') .
                    '<div class="maximized-item">' .
                    ($countItem == 1 && $status == "Em preparo" ?
                        '<div class="alert">
                                    <div>
                                        <svg class="not-wl" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M8 0C3.584 0 0 3.584 0 8C0 12.416 3.584 16 8 16C12.416 16 16 12.416 16 8C16 3.584 12.416 0 8 0ZM6.4 12L2.4 8L3.528 6.872L6.4 9.736L12.472 3.664L13.6 4.8L6.4 12Z" fill="#25A65B" />
                                        </svg>
                                        <span>' . end($itens['pedido']['status'])['descricao'] . '!</span>
                                    </div>
                                </div>'
                        : '') .
                    '<div class="forecast">
                        <div class="forecast-minimized">
                            <p class="forecast-minimized-title">' . ($itens['pedido']['dataProgramada'] != "" ? 'Agendado' : 'Previsão') . '</p>
                            <p class="forecast-minimized-subtitle">' . $itens['utils']['previsaoEntrega'] . '</p>
                            <div class="forecast-minimized-button">
                                <p></p>
                                <svg width="16" height="20" viewBox="0 0 16 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11.2848 8.42863L12 9.12199L8 13.0001L4 9.12199L4.71515 8.42863L8 11.6134L11.2848 8.42863Z" fill="#E4002B"/>
                                </svg>
                            </div>
                        </div>
                        <div class="forecast-maximized">
                            <div class="divider"></div>';

                foreach ($itens['pedido']['status'] as $value) {
                    $bill .=
                        '<div class="forecast-maximized-row">
                            <p>' . $value['descricao'] . '</p>
                            <p>' . explode("-", $value['dataHoraInsert'])[1] . '</p>
                        </div>';
                }

                $bill .=
                    '</div>
                    </div>
                    <div class="itens">
                            <div class="itens-1">
                            ' . $itens[1] . '
                            </div>
                            <div class="itens-2">
                            ' . $itens[2] . '
                            </div>
                        </div>
                        <p class="payment-title">Pagamento</p>
                        <div class="payment-subtotal">
                            <span>Subtotal:</span>
                            <span>R$ ' . self::price($itens['pagamento']['valorCompra']) . '</span>
                        </div>';

                if (!$retirada) {
                    $bill .=
                        '<div class="payment-frete">
                            <span>Frete:</span>
                            <span>R$ ' . self::price($itens['pagamento']['valorFrete']) . '</span>
                        </div>';
                }


                if (isset($itens['pagamento']['cupomCompra'])) {
                    $bill .=
                        '<div class="payment-subtotal">
                            <span>Cupom:</span>
                            <span>' . $itens['pagamento']['cupomCompra'] . '</span>
                        </div>
                        <div class="payment-subtotal">
                            <span>' . $itens['pagamento']['cupomDesconto'] . '</span>
                        </div>';
                }

                $bill .=
                    '<div class="payment-total">
                            <span>Total:</span>
                            <span>R$ ' . self::price($itens['pagamento']['valorTotal']) . '</span>
                        </div>
                    <div class="divider"></div>' .

                    //Tipo pagamento
                    (!$retirada ? View::render('utils/online-bill/payment-type-area', [
                        "tipoPagamento" => $itens['utils']['tipoPagamento'],
                        "flagImg" => parent::lowerSpace($itens['pagamento']['bandeiraCartao'] ?? $itens['pagamento']['metodoPagamento']),
                        "metodoPagamento" => (isset($itens['pagamento']['bandeiraCartao']) && $itens['pagamento']['bandeiraCartao'] != '' ? $itens['pagamento']['bandeiraCartao'] . ' - ' : '') . $itens['pagamento']['metodoPagamento'],
                        "troco" => (isset($itens['pagamento']['troco']) && $itens['pagamento']['troco'] > 0 ? '<p class="payment-type-title-extra">Você receberá R$ ' . parent::price($itens['pagamento']['troco']) . ' de troco</p>' : '')
                    ]) : '') .

                    //Endereço da entrega ou da retirada
                    (!$retirada ? View::render('utils/online-bill/address-area', [
                        "title" => "Entrega",
                        "nomeUsuario" => $itens['cliente']['nomeUsuario'],
                        "telefoneUsuario" => $itens['cliente']['telefone'],
                        "endereco" => $itens['cliente']['rua'] . ' ' . $itens['cliente']['numero'] . ' - ' . $itens['cliente']['bairro'] . ', ' . $itens['cliente']['cidade'] . ' - ' . $itens['cliente']['estado'] . ', ' . $itens['cliente']['cep'],
                    ]) :
                        View::render('utils/online-bill/address-area-withdraw', [
                            "title" => "Retirada",
                            "nomeUsuario" => $itens['cliente']['nomeUsuario'],
                            "telefoneUsuario" => $itens['cliente']['telefone'],
                            "endereco" => BasicInfo::getData('endereco'),
                            "enderecoURL" => str_replace(".", "", str_replace(" ", "+", BasicInfo::getData('endereco')))
                        ])) .

                    ($countItem > 1 ? View::render('utils/online-bill/expandable-buttom-close') : '') . '</div>';

                $countItem++;
            }
        }

        echo $bill;
    }
}
