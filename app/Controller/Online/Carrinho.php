<?php

namespace App\Controller\Online;

use App\Controller\Page;
use App\Model\Address;
use App\Model\Cart;
use App\Session\Admin\BasicInfo;
use App\Session\Admin\Cliente;
use App\Session\Admin\Pagamento;
use App\Session\Admin\Pedido;
use App\Session\Admin\ProdutoFood;
use App\Utils\View;

class Carrinho extends Page
{
    public static function getCart()
    {
        $view = 'online/carrinho';

        $address = Cliente::getObject();
        $pedido = Pedido::getObject();
        $frete = Address::getFrete();
        $basicInfo = BasicInfo::getObject();

        if ($basicInfo['disponivel'] != "sim")
            die("402");

        //Montando as informações de frete, previsão de entrega
        $addressExists = '';
        $freteValue = 0;
        if (
            $address != null && $address['nomeUsuario'] != null && $address['telefone'] != null &&
            $frete['statusCode'] == 200 && $frete['items'] != null ||
            $pedido['retirada']
        ) {
            $freteValue = $frete['items']['valorFrete'];
            $addressComplete = $address['rua'] . ' ' . $address['numero'] . ' ' . ($address['bairro'] != '' ? ' - ' . $address['bairro'] : '') . ', ' . $address['cidade'];

            //Verificando se é compra pra entrega,
            if ($pedido['retirada'])
                $addressExists = View::render('utils/online-cart/forecast-delivery', ["withdraw" => 'withdraw', "address" => $basicInfo['endereco'], "name" => $address['nomeUsuario'], "phone" => $address['telefone']]);
            else
                $addressExists = View::render('utils/online-cart/forecast-delivery', ["withdraw" => '', "address" => $addressComplete]);

            //Verificando se é compra normal ou agendada
            $dataProgramada = Pedido::getData('dataProgramada');
            $verify = Cart::verifySchedule($dataProgramada);
            if ($verify['statusCode'] != 200) {
                $dataProgramada = null;
                Pedido::removeData(['dataProgramada', 'dataProgramadaTexto']);
            }

            if ($dataProgramada == null) {
                $titleForecast = $pedido['retirada'] ? 'Previsão para retirada' : 'Previsão';
                $addressExists .= View::render('utils/online-cart/info-delivery', ["title" => $titleForecast, "previsaoEntrega" => $frete['items']['previsaoEntrega']]);
            } else
                $addressExists .= View::render('utils/online-cart/info-schedule', ["dateText" => Pedido::getData('dataProgramadaTexto')]);
        } else if ($address != null && $frete['statusCode'] == 200 && $frete['items'] != null)
            Cliente::clearAddress();

        //Informação de frete do carrinho
        $freteInfo = '';
        if ($addressExists == '')
            $freteInfo = '<div class="freteInfo"><span>Frete:</span><span>R$ 0,00</span></div>';
        else if ($pedido['retirada'])
            $freteInfo = '';
        else if ($freteValue > 0)
            $freteInfo = '<div class="freteInfo"><span>Frete:</span><span>R$ ' . self::price($freteValue) . '</span></div>';
        else if ($freteValue <= 0)
            $freteInfo = '<div class="freteInfo"><span>Frete:</span><span class="free-shipping">GRÁTIS!</span></div>';

        //Montando a lista de produtos
        $carrinhoList = ProdutoFood::getProdutoFood()['carrinhoList'];
        $itens[1] = '';
        $itens[2] = '';
        if ($carrinhoList != null) {

            $valueFinal = 0;
            $pos = 1;

            foreach ($carrinhoList as $indexCl => $valueCl) {

                if ($valueCl != null) {

                    $value = $valueCl['produto'][0];

                    $item = "<div class='item-pedido' ref='" . $indexCl . "'><div class='pedido-produto'><img class='product-image' src='" . parent::https($value['url']) . "' onerror=this.src='/cardapio.ai/img/product-no-photo.svg'><div class='description'><span class='product-title'>" . $value['nomeProduto'] . "</span><div class='buttons'><div class='buttons-elegant elegant' ref='" . $indexCl . "' max='' min='0'><div> <svg width='12' height='12' viewBox='0 0 12 12' fill='none' xmlns='http://www.w3.org/2000/svg'> <rect y='5' width='12' height='2' fill='#E4002B'/> </svg> </div> <div> <span id='quantity-value'>" . $valueCl['quantidade'] . "</span> </div> <div> <svg width='12' height='12' viewBox='0 0 12 12' fill='none' xmlns='http://www.w3.org/2000/svg'> <path d='M5 0H7V12H5V0Z' fill='#E4002B'/> <path d='M12 5V7L0 7L8.74228e-08 5L12 5Z' fill='#E4002B'/> </svg> </div> </div> <div class='value-area' ref='" . $indexCl . "'> <span>R$</span> <span>" . self::price($valueCl['valorFinal']) . "</span> </div> </div> </div></div>";

                    if (isset($valueCl['ingredientesEscolhidos'])) {
                        $item .= "<div class='pedido-extra'> <span class='title'>Ingredientes removidos</span>";
                        foreach ($valueCl['ingredientesEscolhidos'] as $indexIng => $valueIng) {
                            if (isset($valueIng['quantidadeAlterada']))
                                $item .= " <span>- " . $valueIng['quantidadeAlterada'] . "x " . $valueIng['nomeProduto'] . "</span> ";
                        }
                        $item .= "</div>";
                    }

                    $multipleChoice = '';
                    $uniqueChoice = '';
                    if (isset($valueCl['adicionais'])) {
                        foreach ($valueCl['adicionais'] as $indexAdi => $valueAdi) {
                            if ($valueAdi['quantidade'] == "0")
                                $uniqueChoice .= " <span>• " . $valueAdi['nomeProduto'] . "</span> ";
                            else
                                $multipleChoice .= " <span>+ " . $valueAdi['quantidade'] . "x " . $valueAdi['nomeProduto'] . "</span> ";
                        }
                    }

                    if ($multipleChoice != '') {
                        $item .= "<div class='pedido-extra'> <span class='title'>Adicionais</span>" . $multipleChoice . "</div>";
                    }

                    if ($uniqueChoice != '') {
                        $item .= "<div class='pedido-extra'> <span class='title'>Seção obrigatória</span>" . $uniqueChoice . "</div>";
                    }

                    if (isset($valueCl['notasAdicionais'])) {
                        $item .= "<div class='pedido-extra observacao'> <span class='title'>Observação</span> <span>" . $valueCl['notasAdicionais'] . "</span> </div>";
                    }

                    $item .= "</div>";

                    $itens[$pos] .= $item;

                    if ($pos == 1)
                        $pos = 2;
                    else
                        $pos = 1;

                    $valueFinal += $valueCl['valorFinal'];
                }
            }
        } else
            die("401");

        //Montando as informações do valor minimo de pedido
        if ($basicInfo != null) {
            $valorMinimo = $basicInfo['valorMinimo'] ?? 0;

            $minimumOrder = '';
            if ($valorMinimo > 0 && $valueFinal < $valorMinimo) {
                $minimumOrder =
                    '<div class="value-online"> 
                        <span>O pedido mínimo é de </span> <span>R$ ' . self::price($valorMinimo) . '.</span> 
                    </div>';
            }
        }

        //Montando as informações do tipo de pagamento
        $payment = Pagamento::getObject();
        $paymentArea = '';
        $typePayment = isset($payment['bandeiraCartao']) && $payment['bandeiraCartao'] != '' ? $payment['bandeiraCartao'] : $payment['metodoPagamento'];
        if ($payment != null && $payment['metodoPagamento'] != null) {
            $paymentArea =
                '<div class="payment-method" stage="payment">
                ' . ($payment['tipoPagamento'] == 'delivery' ? '<p>Pagamento na entrega</p>' : '') . '
                    <div class="d-flex">
                        <img src="/cardapio.ai/img/card-flags/' . parent::lowerSpace($typePayment) . '.svg" onerror=this.src="/cardapio.ai/img/card-flags/sem_bandeira.svg">
                        <p>' . (isset($payment['bandeiraCartao']) && $payment['bandeiraCartao'] != '' ? $payment['bandeiraCartao'] . ' - ' : '') . $payment['metodoPagamento']

                . ($payment['tipoPagamento'] == 'online' ? '<p>&nbsp;•••• ' . substr($payment['number'], -4) . '</p>' : '</p>') .

                ($payment['metodoPagamento'] == 'Dinheiro' && $payment['valorPagoDinheiro'] !== null && ($payment['valorPagoDinheiro'] - ($valueFinal + $freteValue)) > 0 ?
                    '<p>. Troco para R$ ' . self::price($payment['valorPagoDinheiro']) . '</p>' : ($payment['metodoPagamento'] == 'Dinheiro' && $payment['valorPagoDinheiro'] !== null && ($payment['valorPagoDinheiro'] - ($valueFinal + $freteValue)) == 0 ?
                        '<p>, R$ ' . self::price($valueFinal + $freteValue) : ($payment['metodoPagamento'] == 'Dinheiro' && $payment['valorPagoDinheiro'] == 0 ?
                            '<p>. Troco não solicitado.' :
                            '<p></p>'))) .

                '<p class="payment-method-button">Mudar</p>
                    <svg width="6" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.749999 0.929637L1.54627 0.125092L6 4.62509L1.54627 9.12509L0.749999 8.32055L4.40745 4.62509L0.749999 0.929637Z" fill="#E4002B"></path>
                    </svg>
                </div>
                ' . ($payment['metodoPagamento'] == 'Dinheiro' && $payment['valorPagoDinheiro'] !== null && ($payment['valorPagoDinheiro'] - ($valueFinal + $freteValue)) > 0 ? '<p class="payment-method-text">Você receberá R$ ' . self::price($payment['valorPagoDinheiro'] - ($valueFinal + $freteValue)) . ' de troco</p>' : '') . '
            </div>';
        }

        //Exibir modal de troco
        $extra = '';
        if (
            $payment['metodoPagamento'] == 'Dinheiro' && !isset($payment['valorPagoDinheiro']) ||
            $payment['metodoPagamento'] == 'Dinheiro' && $payment['valorPagoDinheiro'] < 0
        ) {
            $extra = View::render('utils/online-cart/bottom-modal', [
                "value" => self::price($valueFinal + $freteValue)
            ]);
        }

        //Horarios disponiveis
        $timesItem = '';
        foreach ($basicInfo['horarios'] as $key => $value) {
            if ($value != null) {
                $timesItem .=
                    '<p>' . $value['titulo'] . '</p>
                    <p class="opening-normal">' . $value['abre'] . ' às ' . $value['fecha'] . '</p>';
            }
        }

        //Tipos Pagamentos
        $imgPaymentType = ["credito", "debito", "vavr", "vavr", "dinheiro"];
        $paymentOptionRows = '';
        if (sizeof($basicInfo['tiposPagamentos']) > 0) {

            foreach ($basicInfo['tiposPagamentos'] as $key => $value) {
                if ($value != null) {
                    $cod = $value['codigoPagamento'];

                    //Verificando se existem cartões especificos
                    $paymentOptionRow_cards = '';
                    $hasCards = '';
                    if (isset($value['cartoes'])) {
                        $hasCards = ' cards';

                        foreach ($value['cartoes'] as $keyCartoes => $valueCartoes) {
                            if ($valueCartoes != null) {
                                $paymentOptionRow_cards .= View::render('utils/online-cart/paymentOptionRow-cards', [
                                    "nome" => $valueCartoes,
                                    "img" => parent::lowerSpace($valueCartoes)
                                ]);
                            }
                        }
                    }

                    $paymentOptionRows .= View::render('utils/online-cart/paymentOptionRow', [
                        "id" => $cod,
                        "img" => $imgPaymentType[$cod],
                        "tipoPagamento" => $value['tipoPagamento'],
                        "paymentOptionRow-cards" => $paymentOptionRow_cards,
                        "hasCards" => $hasCards
                    ]);
                }
            }
        }

        //Etapa atual do fluxo de checkout
        $stage = 'address';
        if ($addressExists != '' && !$pedido['retirada'])
            $stage = 'payment';
        if ($paymentArea != '' && $addressExists != '' || $pedido['retirada'] && $addressExists/*&& parent::getPath() == 'online'*/)
            $stage = 'finish';

        //Verificando se o plano do cliente possui cupom
        $couponArea = '';
        if ($basicInfo['plano'] != 'vitrineKey')
            $couponArea = View::render('utils/online-cart/coupon-area', [
                'coupon' => $payment['coupon'] ?? ''
            ]);

        //Javascript extra
        $extra .=
            '<script>
            buttonText();
            $(".datepicker").datepicker();
            $(".is-today").addClass("is-selected");
            $(".timepicker").timepicker();
            if($(".coupon-field input[type=text]").val() != ""){
                $(".coupon-area-button").click();
                $(".coupon-field-button").click();
            }
        </script>';

        $content = View::render($view, [
            //Sessions
            'address-area' => View::render('utils/online-cart/address-area'),
            'schedule-area' => View::render('utils/online-cart/schedule-area'),
            'paymentondelivery-area' => View::render('utils/online-cart/paymentondelivery-area'),
            'withdraw-area' => View::render('utils/online-cart/withdraw-area'),
            //Variables
            'addressExists' => $addressExists . $minimumOrder,
            'itens1' => $itens[1],
            'itens2' => $itens[2],
            'subtotal' => "R$ " . self::price($valueFinal),
            'valueFinal' => "R$ " . self::price($valueFinal + $freteValue),
            'frete' => $freteValue,
            'freteInfo' => $freteInfo,
            'paymentArea' => $paymentArea,
            'stage' => $stage,
            'stageMain' => $basicInfo['retirada'] ? $stage : '',
            'horariosDisponiveis' => $timesItem,
            'paymentOptionRows' => $paymentOptionRows,
            //Address
            'nome' => $address['nomeUsuario'] ?? '',
            'telefone' => $address['telefone'] ?? '',
            'cep' => $address['cep'] ?? '',
            'rua' => $address['rua'] ?? '',
            'numero' => $address['numero'] ?? '',
            'bairro' => $address['bairro'] ?? '',
            'cidade' => $address['cidade'] ?? '',
            'estado' => $address['estado'] ?? '',
            'complemento' => $address['complemento'] ?? '',
            //Withdraw
            'endereco' => $basicInfo['endereco'],
            //Coupon
            'couponArea' => $couponArea
        ]);

        return parent::getPageOnly($view, $content, $extra);
    }

    public static function updateObject()
    {
        return ProdutoFood::updateProdutoFood($_POST['index'], $_POST['qtd'], $_POST['f']);
    }

    public static function setAddress()
    {
        $cliente = json_decode($_POST['cliente'], true);

        Cliente::setObject([
            "nomeUsuario" => $cliente['nome'],
            "telefone" => $cliente['telefone'],
            "cep" => $cliente['cep'],
            "bairro" => $cliente['bairro'],
            "cidade" => $cliente['cidade'],
            "estado" => $cliente['estado'],
            "complemento" => $cliente['complemento'],
            "numero" => $cliente['numero'],
            "pais" => "Brasil",
            "rua" => $cliente['endereco']
        ]);

        $frete = Address::getFrete(false);

        if ($frete['statusCode'] == 400) {
            Cliente::clearAddress();
            die(header("HTTP/1.0 404 Not Found"));
        } else {
            Pedido::setData([
                "retirada" => false
            ]);
        }
    }

    public static function latLongAddress()
    {
        return json_encode(Address::latLongAddress($_POST));
    }

    public static function getAddress()
    {
        return json_encode(Cliente::getObject());
    }

    public static function setPaymentMethod()
    {
        $post = $_POST;
        $data = [];

        $type = $post['val_1'] == 'paymentOptionRow' ? 'delivery' : 'online';
        $index = $post['val_6'];
        if ($type == 'online')
            $method = 'Crédito';
        else if ($type == 'delivery') {
            if ($index == 0)
                $method = 'Crédito';
            else if ($index == 1)
                $method = 'Débito';
            else if ($index == 2)
                $method = 'VA';
            else if ($index == 3)
                $method = 'VR';
            else if ($index == 4)
                $method = 'Dinheiro';
        }

        $data['tipoPagamento'] = $type;
        $data['metodoPagamento'] = $method;
        $data['bandeiraCartao'] = BasicInfo::getData('tiposPagamentos')[$index]['cartoes'][$post['val_7']] ?? null;

        Pagamento::setPaymentMethod($data);
    }

    public static function getPaymentMethod()
    {
        return json_encode(Pagamento::getObject());
    }

    public static function setChange()
    {
        $change = $_POST['val_8'] ?? 0;

        $totalValue = ProdutoFood::getTotalValue();
        if ($change <= $totalValue)
            $change = 0;

        Pagamento::setData([
            'valorPagoDinheiro' => (float)$change
        ]);

        if ($change > 0) {
            return json_encode(array(
                'troco' => $change - $totalValue,
                'value' => $change
            ));
        } else
            return json_encode(array(
                'troco' => 0
            ));
    }

    public static function updateChange()
    {
        if (Pagamento::getData('metodoPagamento') == 'Dinheiro' && Pagamento::getData('valorPagoDinheiro') > 0) {
            $cupom = Pagamento::getData('cupomCompra') ?? '';
            $total = ProdutoFood::getTotalValue(false);
            $frete = Address::getFreteValue();
            $change = Pagamento::getData('valorPagoDinheiro');

            if ($cupom != '') {
                $cupom = Cart::verifyCoupon($cupom);

                if ($cupom['item'] != null && $total > $cupom['item']['valorMinimoCompra']) {
                    if ($cupom['item']['valorPorcentagem'] > 0)
                        $total =  $total * (1 - ($cupom['item']['valorPorcentagem'] / 100));

                    if ($cupom['item']['valorReal'] > 0)
                        $total = $total - $cupom['item']['valorReal'];

                    if ($cupom['item']['freteGratis'])
                        $frete = 0;

                    //Não é permitido valor total negativo
                    if ($total < 0)
                        $total = 0;
                }
            }

            if (($change - ($total + $frete)) < 0)
                Pagamento::removePaymentMethod();

            return $change - ($total + $frete);
        } else {
            return '';
        }
    }

    public static function verifyCoupon()
    {
        $cupom = Cart::verifyCoupon($_POST['cupom']);

        $subTotal = ProdutoFood::getTotalValue(false);
        $frete = Address::getFreteValue();

        if ($cupom['item'] != null && $subTotal > $cupom['item']['valorMinimoCompra']) {

            Pagamento::setCoupon($cupom['item']['nomeCupom']);

            $total = $subTotal;
            $discount = '';
            if ($cupom['item']['valorPorcentagem'] > 0) {
                $value = $subTotal * ($cupom['item']['valorPorcentagem'] / 100);
                $value = $value > $subTotal ? $subTotal : $value;

                $discount = '<div class="discount"><span>Desconto:</span><span>- R$ ' . self::price($value) . '</span></div>';
                $total =  $subTotal * (1 - ($cupom['item']['valorPorcentagem'] / 100));
            }

            if ($cupom['item']['valorReal'] > 0) {
                $value = $cupom['item']['valorReal'];
                $value = $value > $subTotal ? $subTotal : $value;

                $discount = '<div class="discount"><span>Desconto:</span><span>- R$ ' . self::price($value) . '</span></div>';
                $total = $subTotal - $cupom['item']['valorReal'];
            }

            //Não é permitido valor total negativo
            if ($total < 0)
                $total = 0;

            return json_encode(array(
                'subTotal' => self::price($subTotal),
                'frete' => $cupom['item']['freteGratis'] ? self::price(0) : self::price($frete),
                'total' => self::price((!$cupom['item']['freteGratis'] ? $total + $frete : $total)),
                'textMessage' => '<p class="coupon-message">' . $cupom['item']['cupomMessage'] . '</p>',
                'class' => 'success',
                'discount' => $discount
            ));
        } else {

            self::cleanCoupom();

            $error_message = $cupom['item'] == null
                ? $cupom['statusMessage']
                : ($subTotal <= $cupom['item']['valorMinimoCompra']
                    ? 'O subtotal deve ser maior que R$ ' . parent::price($cupom['item']['valorMinimoCompra'])
                    : 'Cupom inválido');

            return json_encode(array(
                'subTotal' => self::price($subTotal),
                'frete' => self::price($frete),
                'total' => self::price(($subTotal + $frete)),
                'textMessage' => '<p class="coupon-message">' . $error_message  . '</p>',
                'class' => 'error'
            ));
        }
    }

    public static function setSchedule()
    {
        $date = $_POST['date'];
        $verify = Cart::verifySchedule($date);

        if ($verify['statusCode'] == 200) {

            if ($verify['items'] != null && sizeof($verify['items']) > 0)
                Pedido::setSchedule($verify['items']);

            return json_encode($verify);
        } else
            parent::status($verify);
    }

    public static function cleanCoupom()
    {
        Pagamento::setCoupon(false);
    }

    public static function setToken()
    {
        $token = $_POST['token'];
        Cliente::setData(['tokenNotificacao', $token]);
    }

    public static function finishCart()
    {
        //O pedido do checkout não deve ter informações de mesa
        //O objeto de pedido deve conter se é retirada ou não
        Pedido::removeData(['mesa']);

        Pedido::setData([
            "retirada" => Pedido::getData("retirada") ?? false
        ]);

        $data = Cart::registerBillCheckout();
        $response = $data["response"];

        if ($response["statusCode"] == 200) {
            ProdutoFood::clearProdutoFood();
            Pagamento::clearObject();
            Pedido::clearObject();
        } else {
            Cart::registrarErro([
                "title" => "Venda incompleta registrada",
                "json" => $data["json"],
                "retornoApi" => $response["statusMessage"]
            ]);
        }

        return $response;
    }

    public static function openArea()
    {
        $retirada = BasicInfo::getData('retirada');
        $response = [];
        $response['append'] = "$('.cart-area').hide();";
        $page = $_POST['page'];

        if ($page == 'address') {

            $response['area'] = 'address-area';
        } else if ($page == 'typeDelivery') {
            if ($retirada) {
                $modal = parent::breakLine(self::modalTypeCart());
                $response['area'] = 'cart-area';
                $response['append'] = "$(document).scrollTop($(document).height());$('.cart-section').append('" . $modal . "')";
            } else
                $response['area'] = 'address-area';
        } else if ($page == 'payment') {

            $response['area'] = 'paymentondelivery-area';
        } else if ($page == 'withdraw') {
            if ($retirada)
                $response['area'] = 'withdraw-area';
            else
                $response['area'] = 'address-area';
        } else if ($page == 'finish') {

            $finishCart = self::finishCart();
            if ($finishCart["statusCode"] == 200) {
                $response['append'] .=
                    "goTo(2);
                    $('.bottom-bar').show();
                    $('.cart-section *').remove();";
            } else {
                $response['area'] = 'cart-area';
                $response['append'] = "showAlert('" . ($finishCart["statusMessage"] ?? "Não conseguimos processar seu pedido.") . "', '')";
            }
        }

        return json_encode($response);
    }

    public static function modalTypeCart()
    {
        if (Pedido::getData('retirada')) {
            return View::render('utils/online-cart/modal-type-cart-withdraw', [
                "endereco" => BasicInfo::getData('endereco'),
                "nome" => Cliente::getData('nomeUsuario'),
                "telefone" => Cliente::getData('telefone'),
            ]);
        } else {
            $address = Cliente::getObject();

            return View::render('utils/online-cart/modal-type-cart-delivery', [
                "endereco" => $address['rua'] . ', ' . $address['numero'] . ' - ' . $address['bairro'] . ', ' . $address['cidade'] . ' - ' . $address['estado'] . ' CEP ' . $address['cep'],
                "complemento" => '<p>' . $address['complemento'] . '</p>',
                "nome" => Cliente::getData('nomeUsuario'),
                "telefone" => Cliente::getData('telefone')
            ]);
        }
    }

    public static function wppMessage()
    {
        $address = Cliente::getObject();
        $frete = Address::getFrete();
        $payment = Pagamento::getObject();

        $produtos = '';
        $valueFinal = 0;
        $valueFrete = $frete['items']['valorFrete'];
        $carrinhoList = ProdutoFood::getProdutoFood()['carrinhoList'];
        if ($carrinhoList != null) {
            foreach ($carrinhoList as $indexCl => $valueCl) {
                if ($valueCl != null) {
                    $value = $valueCl['produto'][0];

                    $produtos .= '%0a%0a*' . $valueCl['quantidade'] . 'x ' . $value['nomeProduto'] . ' (R$ ' . parent::price($valueCl['valorFinal']) . ')*%0a';

                    //Verificando se existem adicionais normais e obrigatorios
                    $multipleChoice = '';
                    $uniqueChoice = '';
                    if (isset($valueCl['adicionais'])) {
                        foreach ($valueCl['adicionais'] as $indexAdi => $valueAdi) {
                            if ($valueAdi['quantidade'] == "0")
                                $uniqueChoice .= "   • " . $valueAdi['nomeProduto'] . "%0a";
                            else
                                $multipleChoice .= "   +" . $valueAdi['quantidade'] . "x " . $valueAdi['nomeProduto'] . "%0a";
                        }
                    }

                    //Adicionais
                    if ($multipleChoice != '') {
                        $produtos .= '   ------------------------%0a   Adicionais:%0a' . $multipleChoice;
                    }

                    //Ingredientes removidos
                    if (isset($valueCl['ingredientesEscolhidos'])) {
                        $produtos .= '   ------------------------%0a   Ingredientes removidos:%0a';
                        foreach ($valueCl['ingredientesEscolhidos'] as $indexIng => $valueIng) {
                            if (isset($valueIng['quantidadeAlterada']))
                                $produtos .= "   -" . $valueIng['quantidadeAlterada'] . "x " . $valueIng['nomeProduto'] . "%0a";
                        }
                    }

                    //Seção de escolha única
                    if ($uniqueChoice != '') {
                        $produtos .= '   ------------------------%0a   Seção obrigatória:%0a' . $uniqueChoice;
                    }

                    //Notas adicionais
                    if (isset($valueCl['notasAdicionais'])) {
                        $produtos .= '   ------------------------%0a   Notas adicionais:%0a   ' . $valueCl['notasAdicionais'] . '%0a';
                    }
                }

                $valueFinal += $valueCl['valorFinal'];
            }
        }

        $endereco =
            $address['rua'] . ', ' . $address['numero'] . ($address['bairro'] != '' ? ' - ' . $address['bairro'] : '') . ', ' . $address['cidade'] . ' - ' . $address['estado'] . ' CEP ' . $address['cep'];

        if (isset($address['complemento']) && $address['complemento'] != '') {
            $complemento =
                "%0a%0aComplemento:%0a" . $address['complemento'];
        }

        $troco = $payment['valorPagoDinheiro'] != null && $payment['valorPagoDinheiro'] > 0 ? ', troco para R$ ' . parent::price($payment['valorPagoDinheiro']) . ' ' : '';

        $message = View::render('utils/online-cart/whatsapp-message', [
            "hora" => date('H:i'),
            "horaPrevista" => $frete['items']['horaPrevistaEntrega'],
            "nome" => $address['nome'],
            "telefone" => $address['telefone'],
            "endereco" => $endereco,
            "complemento" => $complemento,
            "produtos" => $produtos,
            "tipoPagamento" => $payment['metodoPagamento'] . $troco,
            "subtotal" => parent::price($valueFinal),
            "frete" => parent::price($valueFrete),
            "total" => parent::price($valueFinal + $valueFrete),
        ]);

        return parent::breakLine($message);
    }

    public static function setWithdraw()
    {
        $data = $_POST;

        Pagamento::clearObject();
        Cliente::setObject([
            "nomeUsuario" => $data['nome'],
            "telefone" => $data['telefone']
        ]);
        Pedido::setData([
            "retirada" => true
        ]);
    }
}
