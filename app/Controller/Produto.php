<?php

namespace App\Controller;

use App\Controller\Page;
use App\Model\Cart;
use App\Model\ProdutoFood;
use App\Session\Admin\ProdutoFood as AdminProdutoFood;
use App\Utils\View;

class Produto extends Page
{
    public static function getProductByCod()
    {
        $view = 'produto';

        $value = ProdutoFood::getProdutoByCod($_POST['cod']);
        $value = $value['item'];

        AdminProdutoFood::setProduto($value);

        $categorias = array();
        if (isset($value['categorias'])) {
            foreach ($value['categorias'] as $key => $categoria) {
                $categorias[$categoria['id']]['categoria'] = $categoria['obrigatorio'];
                $categorias[$categoria['id']]['html'] =
                    "<div class='chooses'>
                        <div class='title-adicionais'>
                            <div class='title-description'>
                                <span>
                                    " . $categoria['id'] . "
                                </span>";
                if ($categoria['obrigatorio']) {
                    $categorias[$categoria['id']]['html'] .=
                        "<span>
                            Escolha uma opção. (obrigatório)
                        </span>";
                } else {
                    if ($categoria['quantidade'] > 1) {
                        $categorias[$categoria['id']]['html'] .=
                            "<span>
                                Escolha até " . $categoria['quantidade'] . " opções
                            </span>";
                    } else if ($categoria['quantidade'] == 1) {
                        $categorias[$categoria['id']]['html'] .=
                            "<span>
                                Escolha uma opção
                            </span>";
                    } else {
                        $categorias[$categoria['id']]['html'] .=
                            "<span>
                                Escolha quantas opções quiser
                            </span>";
                    }
                }
                $categorias[$categoria['id']]['html'] .=
                    "</div>
                            </div>
                            <div class='itens'>";
            }
        }

        $ingredientes = array();
        if (isset($value['itensAdicionais'])) {
            foreach ($value['itensAdicionais'] as $key => $adicional) {
                if (isset($adicional['adicionalObrigatorio']) && $adicional['adicionalObrigatorio']) {
                    $categorias[$adicional['categoria']]['html'] .=
                        "<div class='item-container'>
                            <div class='title-choose'>
                                <div class='info'>
                                    <span>
                                    " . ucfirst($adicional['nomeProduto']) . "
                                    </span>";

                    if ($categorias[$adicional['categoria']]['categoria']) {

                        $categorias[$adicional['categoria']]['html'] .=
                            "</div>
                                <p>
                                    <input id='$key' type='radio' class='custom-radio' name='" . str_replace(' ', '', $adicional['categoria']) . "'>
                                    <label for='$key'></label>
                                </p>";
                    } else {

                        $categorias[$adicional['categoria']]['html'] .=
                            "<div><span>+R$&nbsp;</span>
                                <span>" . parent::price($adicional['valorInicial']) . "</span></div>
                                </div>
                                <div class='buttons-elegant-adicionais elegant' key='$key'>
                                    <div>
                                        <svg class='item-disabled' width='16' height='16' viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                            <rect y='6.66675' width='16' height='2.66667' fill='#E4002B' />
                                        </svg>
                                    </div>
                                    <div>
                                        <span>0</span>
                                    </div>
                                    <div>
                                        <svg width='16' height='16' viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                            <path d='M6.66667 0H9.33333V16H6.66667V0Z' fill='#E4002B' />
                                            <path d='M16 6.66667V9.33333L0 9.33333L1.16564e-07 6.66667L16 6.66667Z' fill='#E4002B' />
                                        </svg>
                                    </div>
                            </div>";
                    }

                    $categorias[$adicional['categoria']]['html'] .= "</div></div>";
                } else {
                    $ingredientes[] = $adicional;
                }
            }
        }

        $servePessoas = "";
        if (isset($value['servePessoas']) && $value['servePessoas'] != "") {
            $s = "";
            if ($value['servePessoas'] > 1)
                $s = "s";
            $servePessoas =
                "<span>
                    <img src='/cardapio.ai/img/serve-pessoas.svg' style='margin-right:2px;'>
                    Serve " . $value['servePessoas'] . " pessoa$s
                </span>";
        }

        //Verificando se é um produto combo e se contem produtos
        $ingredients = "";
        if (isset($value['produtosCombo'])) {
            $ingredients .=
                "<div class='chooses ingredientes'>
                    <div class='remove-ingredients'>
                        <div class='title'>
                            <span>
                                Itens do combo
                            </span>
                        </div>
                        <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
                            <path d='M5.78788 6.33333L4 8.10283L14 18L24 8.10283L22.2121 6.33333L14 14.461L5.78788 6.33333Z' fill='#E4002B'/>
                        </svg>
                        <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg' style='display:none;'>
                            <path d='M5.78788 17.6667L4 15.8972L14 6L24 15.8972L22.2121 17.6667L14 9.53899L5.78788 17.6667Z' fill='#E4002B'/>
                        </svg>
                    </div>
                <div class='itens'>";

            foreach ($value['produtosCombo'] as $key => $produto) {
                $ingredients .=
                    '<div class="selected-combo"> 
                        <p class="selected-combo-quantity">' . ($produto['quantidade'] ?? 1) . 'x</p>
                        <img class="image" src="' . $produto['url'] . '" onerror="this.style.opacity=0">
                        <svg class="not-wl image-placeholder" width="112" height="112" viewBox="0 0 112 112" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M112 0H0V112H112V0Z" fill="#F5F5F5"></path> <path opacity="0.5" d="M48.0603 46.0568C48.0603 47.6391 47.4319 49.1565 46.313 50.2754C45.1942 51.3941 43.6767 52.0227 42.0944 52.0227C40.5122 52.0227 38.9947 51.3941 37.876 50.2754C36.7571 49.1565 36.1285 47.6391 36.1285 46.0568C36.1285 44.4746 36.7571 42.9571 37.876 41.8382C38.9947 40.7195 40.5122 40.0909 42.0944 40.0909C43.6767 40.0909 45.1942 40.7195 46.313 41.8382C47.4319 42.9571 48.0603 44.4746 48.0603 46.0568Z" fill="#BFBFBF"></path> <path opacity="0.5" fill-rule="evenodd" clip-rule="evenodd" d="M25.9292 33.8837C27.048 32.765 28.5655 32.1364 30.1477 32.1364H81.8523C83.4345 32.1364 84.952 32.765 86.0709 33.8837C87.1896 35.0025 87.8182 36.52 87.8182 38.1023V59.9015C87.1412 59.8041 86.449 59.7535 85.7449 59.7535C85.0994 59.7535 84.4639 59.796 83.8409 59.8783V38.1023C83.8409 37.5749 83.6314 37.0691 83.2585 36.696C82.8855 36.3231 82.3797 36.1136 81.8523 36.1136H30.1477C29.6203 36.1136 29.1145 36.3231 28.7415 36.696C28.3686 37.0691 28.1591 37.5749 28.1591 38.1023V73.8977C28.1596 73.9508 28.1623 74.0039 28.167 74.0568V71.9091L38.6909 62.5465C39.016 62.2227 39.4431 62.0211 39.8998 61.9762C40.3564 61.9313 40.8146 62.0457 41.1965 62.3L51.7762 69.3477L66.5318 54.592C66.8272 54.2975 67.2078 54.1033 67.6196 54.0369C68.0315 53.9705 68.4538 54.0353 68.8267 54.2221L81.0605 60.5319C75.4003 62.4761 71.3324 67.846 71.3324 74.166C71.3324 76.1898 71.7495 78.1161 72.5025 79.8636H30.1477C28.5655 79.8636 27.048 79.235 25.9292 78.1163C24.8104 76.9975 24.1818 75.48 24.1818 73.8977V38.1023C24.1818 36.52 24.8104 35.0025 25.9292 33.8837Z" fill="#BFBFBF"></path> <path opacity="0.5" d="M77.7607 69.0116L80.5855 66.1868L85.7435 71.3449L90.8977 66.1868L93.7225 69.0116L88.5683 74.1697L93.7225 79.3239L90.8977 82.1487L85.7435 76.9945L80.5855 82.1487L77.7607 79.3239L82.9187 74.1697L77.7607 69.0116Z" fill="#BFBFBF"></path> </svg> 
                            <div> 
                                <p class="selected-combo-title">' . $produto['nomeProduto'] . '</p> 
                                <p class="selected-combo-description">' . $produto['descricao'] . '</p> 
                                <div class="d-flex value">' .
                    (isset($produto['medida']) ? '<p class="measure">' . $produto['medida'] . '</p>' : '') .
                    (isset($produto['servePessoas']) ?
                        '<p class="serves"> 
                                    <svg class="not-wl" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"> <g opacity="0.8" clip-path="url(#clip0)"> <path d="M11.3584 10.8346C10.8688 10.1908 10.2656 9.27269 9.30092 9.27269C8.33503 9.27269 6.77618 9.27269 6.77618 9.27269C6.36577 9.27269 6.03393 9.60453 6.03393 10.0155C6.03393 10.4254 6.36577 10.7578 6.77618 10.7578H8.07512C8.23892 10.7578 8.3719 10.8908 8.3719 11.0546V11.1041C8.3719 11.2679 8.23892 11.4009 8.07512 11.4009H5.8931L3.2106 9.02548C2.96519 8.80848 2.58984 8.83145 2.37224 9.07685C2.15464 9.32226 2.17761 9.69761 2.42301 9.91521C2.42301 9.91521 5.11821 13.2191 5.47724 13.2191L8.99145 13.2185L10.5382 14L12.6066 12.4853C12.606 12.4853 12.4192 12.2296 11.3584 10.8346Z" fill="#4D4D4D"></path> <path d="M12.4597 7.34875H1.39056V8.16052H12.4597V7.34875Z" fill="#4D4D4D"> </path> <path d="M11.822 6.79027C11.8371 6.62767 11.8468 6.46387 11.8468 6.29765C11.8468 3.92765 10.1719 1.95052 7.94153 1.48148C7.98807 1.36059 8.01648 1.23003 8.01648 1.09222C8.01648 0.4902 7.52628 0 6.92426 0C6.32224 0 5.83204 0.4902 5.83204 1.09222C5.83204 1.23003 5.86045 1.35999 5.90699 1.48148C3.67721 1.94992 2.00171 3.92765 2.00171 6.29765C2.00171 6.46387 2.01017 6.62828 2.02649 6.79027H11.822ZM6.55253 1.09283C6.55253 0.887317 6.71935 0.720491 6.92486 0.720491C7.13037 0.720491 7.2972 0.887317 7.2972 1.09283C7.2972 1.21492 7.23434 1.31828 7.14246 1.38658C7.06993 1.38296 6.99861 1.3751 6.92486 1.3751C6.85112 1.3751 6.77919 1.38296 6.70666 1.38658C6.61479 1.31828 6.55253 1.21492 6.55253 1.09283Z" fill="#4D4D4D"></path> </g> <defs> <clipPath id="clip0"> <rect width="14" height="14" fill="white"></rect> </clipPath> </defs> </svg>
                                    Serve ' . $produto['servePessoas'] . ' pessoas 
                                    </p>' : '') .
                    '</div> 
                    </div> 
                    </div>';
            }

            $ingredients .=
                "</div>
            </div>";

            //Verificando se é um produto comum e se tem ingredientes
        } else if ($ingredientes != null) {
            $ingredients .=
                "<div class='chooses ingredientes closed'>
                    <div class='remove-ingredients'>
                        <div class='title'>
                            <span>
                                Deseja remover algum ingrediente?
                            </span>
                            <span style='display: none;'>
                                Escolha uma opção. (obrigatório)
                            </span>
                        </div>
                        <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
                            <path d='M5.78788 6.33333L4 8.10283L14 18L24 8.10283L22.2121 6.33333L14 14.461L5.78788 6.33333Z' fill='#E4002B'/>
                        </svg>
                        <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg' style='display:none;'>
                            <path d='M5.78788 17.6667L4 15.8972L14 6L24 15.8972L22.2121 17.6667L14 9.53899L5.78788 17.6667Z' fill='#E4002B'/>
                        </svg>
                    </div>
                    <div class='itens'>";

            foreach ($ingredientes as $key => $ingrediente) {
                if (isset($ingrediente['adicionalObrigatorio']) && !$ingrediente['adicionalObrigatorio'] || !isset($ingrediente['adicionalObrigatorio'])) {
                    $ingredients .=
                        "<div class='item-container'>
                            <div class='title-choose'>
                                <div class='info'>
                                    <span>
                                        " . $ingrediente['nomeProduto'] . "
                                    </span>
                                    <span style='display: none;'>
                                        Descrição ou preço +R$ 0,00
                                    </span>
                                </div>
                                <div class='buttons-elegant-ingredientes elegant' key='$key'>
                                    <div>
                                        <svg width='16' height='16' viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                            <rect y='6.66675' width='16' height='2.66667' fill='#E4002B' />
                                        </svg>
                                    </div>
                                    <div>
                                        <span>" . $ingrediente['quantidade'] . "</span>
                                    </div>
                                    <div>
                                        <svg class='item-disabled' width='16' height='16' viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                            <path d='M6.66667 0H9.33333V16H6.66667V0Z' fill='#E4002B' />
                                            <path d='M16 6.66667V9.33333L0 9.33333L1.16564e-07 6.66667L16 6.66667Z' fill='#E4002B' />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>";
                }
            }

            $ingredients .=
                "</div>
            </div>";
        }

        $categories = "";
        if ($categorias != null) {
            foreach ($categorias as $key => $categoria) {
                $categories .= $categoria['html'];
                $categories .= "</div></div>";
            }
        }

        $content = View::render($view, [
            'url' => parent::https($value['url']),
            'nomeProduto' => $value['nomeProduto'],
            'medida' => isset($value['medida']) ? "<span>" . $value['medida'] . "</span>" : "",
            'servePessoas' => $servePessoas,
            'descricao' => isset($value['descricao']) && $value['descricao'] != "" ? "<div class='description' style='margin-top: 24px;width: 333px;'> <span> " . $value['descricao'] . " </span> </div>" : "",
            'ingredients' => $ingredients,
            'categorias' => $categories,
            'valorInicial' => number_format((float)$value['valorInicial'], 2, ',', ''),
            'value' => json_encode($value)
        ]);

        return parent::getPageOnly($view, $content);
    }

    public static function updateQtdProduto()
    {
        $postQuantidade = $_POST['option'];

        if ($postQuantidade == 0) {
            return AdminProdutoFood::updateProduto([
                'produto' => true,
                'quantidade' => -1
            ]);
        } else if ($postQuantidade == 2) {
            return AdminProdutoFood::updateProduto([
                'produto' => true,
                'quantidade' => 1
            ]);
        }

        return null;
    }

    public static function updateQtdAdicionais()
    {
        $key = $_POST['key'];
        $postQuantidade = $_POST['option'];

        if ($postQuantidade == 0) {
            return AdminProdutoFood::updateProduto([
                'adicionais' => $key,
                'quantidade' => -1
            ]);
        } else if ($postQuantidade == 2) {
            return AdminProdutoFood::updateProduto([
                'adicionais' => $key,
                'quantidade' => 1
            ]);
        }

        return null;
    }

    public static function updateQtdAdicionaisRadio()
    {
        $key = $_POST['key'];
        return AdminProdutoFood::updateProduto([
            'adicionalRadio' => $key
        ]);
    }

    public static function updateQtdIngredientes()
    {
        $key = $_POST['key'];
        $postQuantidade = $_POST['option'];

        if ($postQuantidade == 0) {
            return AdminProdutoFood::updateProduto([
                'ingrediente' => $key,
                'quantidade' => 1
            ]);
        } else if ($postQuantidade == 2) {
            return AdminProdutoFood::updateProduto([
                'ingrediente' => $key,
                'quantidade' => -1
            ]);
        }

        return null;
    }

    public static function setObject()
    {
        AdminProdutoFood::setProdutoFood($_POST);
    }

    public static function getObject()
    {
        echo json_encode(AdminProdutoFood::getProdutoFood());
    }

    public static function getQuantityObject()
    {
        $object = AdminProdutoFood::getProdutoFood();

        $qtd = 0;
        if ($object != null && $object['carrinhoList'] != null) {
            foreach ($object['carrinhoList'] as $key => $value) {
                $qtd += $value['quantidade'];
            }
        }

        echo $qtd;
    }

    public static function registerErro()
    {
        Cart::registrarErro([
            "title" => $_POST['title'],
            "json" => $_POST['obj']
        ]);
    }
}
