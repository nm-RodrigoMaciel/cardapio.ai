<?php

namespace App\Controller;

use App\Controller\Page;
use App\Model\Address;
use App\Model\ProdutoFood;
use App\Model\WhiteLabel;
use App\Session\Admin\BasicInfo;
use App\Session\Admin\Cliente;
use App\Session\Admin\Estabelecimento;
use App\Session\Admin\Pedido;
use App\Utils\View;

class Inicio extends Page
{
    public static function getInicio($request, $data)
    {
        $view = 'inicio';

        // session_start();
        // session_unset();

        // session_start();
        // echo '<pre>';
        // print_r($_SESSION);
        // exit;
        // echo '</pre>';

        Estabelecimento::estabelecimento($data);
        $jsonDataProd = ProdutoFood::getProdutos();

        $cont = 0;
        $categories_start = "";
        $products = array();
        $products_tmp = array();
        $categories_search = "";
        $productsFinal = array();
        $listProduct = '';
        if (isset($jsonDataProd) && sizeOf($jsonDataProd) > 0 && !isset($jsonDataProd['statusCode'])) {
            foreach ($jsonDataProd as $key => $values) {

                foreach ($values as $key => $value) {

                    //Validar se existe servePessoas
                    $servePessoas = "";
                    if (isset($value['servePessoas']) && $value['servePessoas'] > 0) {
                        $servePessoas =
                            "<div>
                                <img src='/cardapio.ai/img/serve-pessoas.svg'>
                                <span>" . $value['servePessoas'] . "</span>
                            </div>";
                    }

                    //Validar se existe medidaProduto
                    $medidaProduto = "";
                    if (isset($value['medida'])) {
                        $medidaProduto =
                            "<div>
                                <span>" . $value['medida'] . "</span>
                            </div>";
                    }

                    //Verificando se o produto está em promoção
                    $productValue = $value['valorInicial'];
                    $productPromoValue = "0.00";

                    $productPromo = isset($value['promocao']) && $value['promocao'] != "0";
                    if ($productPromo) {
                        $productPromoValue = ($productValue / ($value['promocao'] / 100));
                    }

                    $product =
                        "<img src='" . parent::https($value['url']) . "?" . time() . "' onerror='this.style.opacity=0'>
                            <svg class='not-wl img-placeholder' width='136' height='136' viewBox='0 0 136 136' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                <path d='M136 0H0V136H136V0Z' fill='#F5F5F5'/>
                                <path opacity='0.5' d='M58.359 55.9261C58.359 57.8474 57.5959 59.6901 56.2372 61.0487C54.8786 62.4072 53.036 63.1705 51.1147 63.1705C49.1934 63.1705 47.3507 62.4072 45.9923 61.0487C44.6337 59.6901 43.8704 57.8474 43.8704 55.9261C43.8704 54.0048 44.6337 52.1622 45.9923 50.8036C47.3507 49.4451 49.1934 48.6818 51.1147 48.6818C53.036 48.6818 54.8786 49.4451 56.2372 50.8036C57.5959 52.1622 58.359 54.0048 58.359 55.9261Z' fill='#BFBFBF'/>
                                <path opacity='0.5' fill-rule='evenodd' clip-rule='evenodd' d='M31.4854 41.1445C32.844 39.786 34.6866 39.0227 36.608 39.0227H99.3921C101.313 39.0227 103.156 39.786 104.515 41.1445C105.873 42.5031 106.636 44.3457 106.636 46.267V72.7376C105.814 72.6192 104.974 72.5579 104.119 72.5579C103.335 72.5579 102.563 72.6095 101.807 72.7093V46.267C101.807 45.6266 101.552 45.0124 101.1 44.5595C100.647 44.1067 100.032 43.8523 99.3921 43.8523H36.608C35.9675 43.8523 35.3534 44.1067 34.9004 44.5595C34.4476 45.0124 34.1932 45.6266 34.1932 46.267V89.733C34.1938 89.7974 34.197 89.8618 34.2028 89.9261V87.3182L46.9818 75.9494C47.3765 75.5562 47.8952 75.3114 48.4497 75.2568C49.0042 75.2023 49.5606 75.3412 50.0244 75.65L62.8711 84.208L80.7886 66.2903C81.1473 65.9326 81.6094 65.6968 82.1095 65.6163C82.6096 65.5356 83.1224 65.6143 83.5752 65.8412L98.4306 73.5031C91.5575 75.8639 86.6179 82.3845 86.6179 90.0587C86.6179 92.5162 87.1244 94.8552 88.0387 96.9773H36.608C34.6866 96.9773 32.844 96.214 31.4854 94.8555C30.1269 93.4969 29.3636 91.6543 29.3636 89.733V46.267C29.3636 44.3457 30.1269 42.5031 31.4854 41.1445Z' fill='#BFBFBF'/>
                                <path opacity='0.5' d='M94.4237 83.7998L97.8539 80.3697L104.117 86.6331L110.376 80.3697L113.806 83.7998L107.547 90.0632L113.806 96.3219L110.376 99.752L104.117 93.4934L97.8539 99.752L94.4237 96.3219L100.687 90.0632L94.4237 83.7998Z' fill='#BFBFBF'/>
                            </svg>
                        <div class='product-info'>
                            <div class='product-header'>
                                <div class='product-title'>
                                    <span>" . ucfirst($value['nomeProduto']) . "</span>
                                    <input id='cod' style='display:none;' value='" . $value['codigoBarras'] . "'>
                                </div>
                                <div class='product-weight'>
                                    $servePessoas
                                    $medidaProduto
                                </div>
                                <div class='product-description'><p>" . $value['descricao'] . "</p></div>
                            </div>
                            <div class='product-value'>
                                <span class='value-final'>R$ " . number_format($productValue, 2, ',', ' ') . "</span>" .
                        ($productPromo ? "<span class='old-value'>R$ " . number_format($productPromoValue, 2, ',', ' ') . "</span>" : "")
                        . "</div>
                        </div>
                        </div>";

                    if (!in_array($value['categoria'], $products_tmp)) {
                        if ($cont == 0) {
                            $categories_start .=
                                "<div class='category selected category-option category-$cont' ref='category-$cont'>
                                    <span>" . ucfirst($value['categoria']) . "</span>
                                </div>";
                        } else {
                            $categories_start .=
                                "<div class='category category-option category-$cont' ref='category-$cont'>
                                    <span>" . ucfirst($value['categoria']) . "</span>
                                </div>";
                        }

                        $categories_search .=
                            "<div class='product category col-6 product-$cont' ref='category-$cont'>
                                <span>" . ucfirst($value['categoria']) . "</span>
                            </div>";

                        $products_tmp[] = $value['categoria'];

                        $products[$value['categoria']][] = [
                            "nome" => $value['nomeProduto'],
                            "categoria" => "<div id='category-$cont' class='title title-category col center-rel-hor'>
                                                <span>" . ucfirst($value['categoria']) . "</span>
                                            </div>"
                        ];
                    }

                    $left = '';
                    if (sizeOf($products[$value['categoria']]) % 2 != 0)
                        $left = 'left';

                    $product = substr_replace($product, "<div class='product product-detais col-12 product-$cont $left' cod='$key'>", 0, 0);

                    $products[$value['categoria']][] = [
                        "nome" => $value['nomeProduto'],
                        "form" => $product
                    ];

                    $cont++;
                }
            }

            foreach ($products as $key => $value) {
                foreach ($value as $key => $value) {
                    $productsFinal[] = $value;
                }
            }

            if (isset($productsFinal)) {
                foreach ($productsFinal as $key => $value) {
                    if (isset($value['categoria']))
                        $listProduct .= $value['categoria'];

                    if (isset($value['form']))
                        $listProduct .= $value['form'];
                }
            }
        }

        //BasicInfo
        BasicInfo::clearObject();
        BasicInfo::setData(WhiteLabel::getBasicInfo()['item']);

        //Mesa
        $mesa = $_GET['mesa'] ?? '';
        Pedido::setData(['mesa' => $mesa]);

        //LinkTree
        $linkTree = '';
        $app = parent::getPath();
        $isRoot = $app != 'online' && $app != 'mesa' && $mesa == '';
        $linkTree = LinkTree::getLinkTree($isRoot, $app == 'mesa');

        $content = View::render($view, [
            'categories_start' => $categories_start,
            'listProduct' => $listProduct,
            'categories_search' => $categories_search,
            'linktree' => $linkTree
        ]);

        return parent::getPage($view, $content);
    }

    public static function simulateShipping()
    {
        $data = $_POST;
        $result = Address::getFreteCep($data);

        if ($result["statusCode"] == 200) {
            Cliente::setData(["cep" => preg_replace('/\D/', '', $data['cep'])]);

            if ($result["items"]["valorFrete"] > 0)
                $taxaEntrega = "Taxa de entrega: R$ " . parent::price($result["items"]["valorFrete"]);
            else
                $taxaEntrega = "Taxa de entrega: <div>Grátis<div>";

            return json_encode([
                "valorFrete" => $taxaEntrega,
                "endereco" => $result["items"]["endereco"]
            ]);
        } else if ($result["statusMessage"] == "O estabelecimento não atende a sua região") {
            return json_encode([
                "valorFrete" => "Fora da área de entrega",
                "endereco" => $result["items"]["endereco"]
            ]);
        } else
            return null;
    }

    public static function planVerify()
    {
        $condition = "";
        $planName = "freePlan";
        if(!BasicInfo::getData('periodoGratis')){
            $safe2pay = WhiteLabel::statusPlanSafe2Pay();

            //Caso tenha um plano pago
            $planName = "vitrineKey";
            if (isset($safe2pay['data']['objects'][0])) {
                //Como se trata do resultado da assinatura, o planName é o idPlan
                $planName = $safe2pay['data']['objects'][0]['planName'];
    
                //Caso esteja em um período de transição, pegar o id do plano que ele está indo
                if (strpos($planName, "ransicao") !== false) {
                    $planName = "Transicao_completoKey_Rodrigo Lanches1663077146";
                    $planName = explode("_", $planName)[1];
                }
            }
                
            $noDesk = "$('body').html('" . parent::breakLine(View::render('utils/block-content/no-permission', ['product' => 'Pedido na mesa'])) . "');";
            $app = APP;
            if ($planName == "vitrineKey") {
    
                if ($app == 'mesa/')
                    $condition .= $noDesk;
            } else if ($planName == "deliveryKey") {
    
                if ($app == 'mesa/')
                    $condition .= $noDesk;
            } else if ($planName == "mesaKey") {
                
            } else if ($planName == "completoKey") {
                
            }
        }

        //Setting o plano na seção
        BasicInfo::setData(['plano' => $planName]);

        return $condition;
    }
}
