<?php

namespace App\Controller;

use App\Session\Admin\BasicInfo;
use App\Session\Admin\Estabelecimento;
use \App\Utils\View;
use DateTime;
use DateTimeZone;

class Page
{
    private function getHeader($view, $extra)
    {
        return View::render('header', [
            'css' => $view,
            'extraHeader' => $extra['extraHeader'] ?? '',
            'favicon' => Estabelecimento::getData('logo'),
            'color' => Estabelecimento::getData('colorPrimary') ?? '#F16B24',
            'title' => Estabelecimento::getData('nomeEstabelecimento') ?? 'Cardápio Digital',
            'pixelFacebook' => BasicInfo::getData('pixelFacebook') != '' ? View::render('utils/pixel-facebook/pixel', ['pixel' => BasicInfo::getData('pixelFacebook')]) : ''
        ]);
    }

    private function getFooter($view, $extra)
    {
        return View::render('footer', [
            'js' => $view,
            'extraFooter' => $extra['extraFooter'] ?? ''
        ]);
    }

    public static function getPage($view, $content, $extra = [])
    {
        return View::render('page', [
            'header' => self::getHeader($view, $extra),
            'content' => $content,
            'footer' => self::getFooter($view, $extra) . ($extra['after'] ?? '')
        ]);
    }

    public static function getPageOnly($view, $content, $extra = '')
    {
        return View::render('page', [
            'header' => '',
            'content' => $content,
            'footer' => $extra
        ]);
    }

    public static function price($value = "")
    {
        if ($value == "")
            $value = 0;

        return number_format($value, 2, ',', '.');
    }

    public static function keyFix($key)
    {
        return preg_replace("/[^a-zA-Z0-9]+/", "", preg_replace('/\s+/', '', strtolower($key)));
    }

    public static function getDateTime()
    {
        $timezone = new DateTimeZone('Brazil/East');
        $datetime = new DateTime();
        $datetime->setTimezone($timezone);
        return strtotime($datetime->format('Y\-m\-d\ h:i:s'));
    }

    public static function status($response)
    {
        header('HTTP/1.1 ' . $response["statusCode"] . ' ' . $response["statusMessage"]);
        header('Content-Type: application/json; charset=UTF-8');
        die($response["statusMessage"]);
    }

    public static function getPath($REQUEST_URI = "")
    {
        if ($REQUEST_URI == "")
            $REQUEST_URI = $_SERVER['REQUEST_URI'];

        return explode("/", $REQUEST_URI)[1];
    }

    public static function breakLine($val)
    {
        return str_replace(array("\r", "\n"), '', $val);
    }

    public static function lowerSpace($val)
    {
        $val = self::removerAcentos($val);
        $val = preg_replace('/[^A-Za-z0-9\- ]/', '', $val);
        $val = preg_replace("/\s+/", "", $val);
        $val = strtolower($val);

        return $val;
    }

    public static function removerAcentos($stringExemplo)
    {
        $comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ü', 'Ú', 'Ç', 'ç');

        $semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'C', 'c');

        return str_replace($comAcentos, $semAcentos, $stringExemplo);
    }

    public static function https($url)
    {
        if (strpos($url, "http://") !== false)
            $url = str_replace("http://", "https://", $url);

        return $url;
    }
}
