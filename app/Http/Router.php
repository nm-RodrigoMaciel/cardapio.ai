<?php

namespace App\Http;

use App\Http\Middleware\Queue as MiddlewareQueue;
use App\Model\WhiteLabel;
use App\Session\Admin\Estabelecimento;
use \Closure;
use Exception;
use \ReflectionFunction;

class Router
{
    private $url = '';
    private $prefix = '';
    private $routes = [];
    private $request;

    public function __construct($url)
    {
        $this->request = new Request($this);
        $this->url = $url;
        $this->setPrefix();
    }

    private function setPrefix()
    {
        $parseUrl = parse_url($this->url);

        $this->prefix = $parseUrl['path'] ?? '';
    }

    private function addRoute($method, $route, $params = [])
    {
        foreach ($params as $key => $value) {
            if ($value instanceof Closure) {
                $params['controller'] = $value;
                unset($params[$key]);
                continue;
            }
        }

        $params['middlewares'] = $params['middlewares'] ?? [];

        $params['variables'] = [];

        $patternVariable = '/{(.*?)}/';
        if (preg_match_all($patternVariable, $route, $matches)) {
            $route = preg_replace($patternVariable, '(.*?)', $route);
            $params['variables'] = $matches[1];
        }

        $patternRoute = '/^' . str_replace('/', '\/', $route) . '$/';

        $this->routes[$patternRoute][$method] = $params;
    }

    public function get($route, $params = [])
    {
        return $this->addRoute('GET', $route, $params);
    }

    public function post($route, $params = [])
    {
        return $this->addRoute('POST', $route, $params);
    }

    public function put($route, $params = [])
    {
        return $this->addRoute('PUT', $route, $params);
    }

    public function delete($route, $params = [])
    {
        return $this->addRoute('DELETE', $route, $params);
    }

    public static function call($jsonData, $path, $external = false)
    {
        $base = Estabelecimento::getData('api');

        $ch = curl_init(($external ? $path : $base . $path));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    public static function callSafe2Pay($url)
    {
        $options = array(
            'http' => array(
                'method'  => 'GET',
                'header' =>  "Content-Type: application/json\r\n" .
                             "x-api-key: CDB0FDF34D084AD5B803DFD3F1B64BB2\r\n"
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, true, $context);

        return json_decode($result, true);
    }

    private function getUri()
    {
        $uri = $this->request->getUri();
        $xUri = strlen($this->prefix) ? explode($this->prefix, $uri) : [$uri];
        return end($xUri);
    }

    private function getRoute()
    {
        $uri = $this->request->getUri();
        $explode = explode('/', $uri);
        $uri = end($explode) != null 
        ? end($explode) 
        : $explode[sizeof($explode) - 2];

        $httpMethod = $this->request->getHttpMethod();
        $userRequest = WhiteLabel::getWhiteLabel($uri);

        foreach ($this->routes as $patternRoute => $methods) {
            if (preg_match($patternRoute, $uri, $matches)) {
                if (isset($methods[$httpMethod])) {
                    unset($matches[0]);

                    $keys = $methods[$httpMethod]['variables'];
                    $methods[$httpMethod]['variables'] = array_combine($keys, $matches);
                    $methods[$httpMethod]['variables']['request'] = $this->request;
                    $methods[$httpMethod]['variables']['data'] = $userRequest;

                    return $methods[$httpMethod];
                }

                throw new Exception("Método não permitido", 405);
            } else if ($userRequest != null) {
                $this->routes['/^\/whitelabel$/'][$httpMethod]['variables']['request'] = $this->request;
                $this->routes['/^\/whitelabel$/'][$httpMethod]['variables']['data'] = $userRequest;
                $this->routes['/^\/whitelabel$/'][$httpMethod]['variables']['data']['path'] = $uri;
                return $this->routes['/^\/whitelabel$/'][$httpMethod];
            }
        }

        throw new Exception('URL não encontrada', 405);
    }

    public function run()
    {
        try {
            $route = $this->getRoute();

            if (!isset($route['controller'])) {
                throw new Exception("A URL não pôde ser processada", 500);
            }

            $args = [];

            $reflection = new ReflectionFunction($route['controller']);
            foreach ($reflection->getParameters() as $parameter) {
                $name = $parameter->getName();
                $args[$name] = $route['variables'][$name] ?? '';
            }

            return (new MiddlewareQueue($route['middlewares'], $route['controller'], $args))->next($this->request);
        } catch (Exception $e) {
            return new Response($e->getCode(), $e->getMessage());
        }
    }

    public function redirect($route)
    {
        $url = $this->url . $route;

        header('location: ' . $url);
        exit;
    }

    public function redirectExternal($url)
    {
        header('location: ' . $url);
        exit;
    }
}
