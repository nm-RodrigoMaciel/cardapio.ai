<?php

namespace App\Model;

use \App\Http\Router;
use App\Session\Admin\Cliente;
use App\Session\Admin\Estabelecimento;
use App\Session\Admin\Pagamento;
use App\Session\Admin\Pedido;
use App\Session\Admin\ProdutoFood;

class Cart
{
    public static function registerBill($carrinhoList, $valueFinal)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "codigoEstabelecimento" => Estabelecimento::getData('codigoEstabelecimento'),
            "tipoEstabelecimento" => Estabelecimento::getData('tipoEstabelecimento'),
            "telefoneEstabelecimento" => Estabelecimento::getData('phone'),
            "tipoPedido" => "mesa",
            "itens" => [
                "pedido" => Pedido::getObject(),
                "cliente" => Cliente::getObject(),
                "carrinhoList" => $carrinhoList,
                "pagamento" => ["valorTotal" => $valueFinal]
            ]
        ];

        return Router::call($jsonData, 'cardapiodigital/registrarpedido/');
    }

    public static function updateBill($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "codigoEstabelecimento" => Estabelecimento::getData('codigoEstabelecimento'),
            "telefone" => $data['phone'],
            "codigoTransacao" => $data['codigoTransacao'],
            "carrinhoList" => $data['carrinhoList'],
            "mesa" => $data['mesa']
        ];

        return Router::call($jsonData, 'cardapiodigital/registrarpedido/');
    }

    public static function verifyOpen($phone)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "codigoEstabelecimento" => Estabelecimento::getData('codigoEstabelecimento'),
            "telefone" => $phone
        ];

        return Router::call($jsonData, 'cardapiodigital/verificaraberto/');
    }

    public static function verifyCoupon($coupon)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "codigoEstabelecimento" => Estabelecimento::getData('codigoEstabelecimento'),
            "cupom" => $coupon
        ];

        return Router::call($jsonData, 'web/cupons/verificarSemValor');
    }

    public static function registerBillCheckout()
    {
        $User = Estabelecimento::getUser();

        $jsonData = [
            "nomeEstabelecimento" => $User['nomeEstabelecimento'],
            "codigoEstabelecimento" => $User['codigoEstabelecimento'],
            "tipoEstabelecimento" => $User['tipoEstabelecimento'],
            "telefoneEstabelecimento" => $User['phone'],
            "tipoPedido" => "delivery",
            "itens" => [
                "pedido" => Pedido::getObject() ?? (object)[],
                "cliente" => Cliente::getObject(),
                "carrinhoList" => ProdutoFood::getProdutoFood()['carrinhoList'],
                "pagamento" => Pagamento::getObject() ?? (object)[]
            ]
        ];

        return [
            "response" => Router::call($jsonData, 'checkout/registrarvenda/'),
            "json" => $jsonData
        ];
    }

    public static function verifyOpenCheckout($cod)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "codigoEstabelecimento" => Estabelecimento::getData('codigoEstabelecimento'),
            "telefone" => $cod
        ];

        return Router::call($jsonData, 'checkout/verificaraberto/');
    }

    public static function verifySchedule($date)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "tipoEstabelecimento" => Estabelecimento::getData('tipoEstabelecimento'),
            "date" => (int)$date
        ];

        return Router::call($jsonData, 'validacao/agendamento/');
    }

    public static function registrarErro($json = [])
    {
        session_start();
        $json["sessao"] = $_SESSION;

        return Router::call($json, 'cardapiodigital/registrarErro/');
    }
}
