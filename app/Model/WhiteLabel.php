<?php

namespace App\Model;

use App\Http\Router;
use App\Session\Admin\Estabelecimento;

class WhiteLabel
{
    public static function getWhiteLabel($user)
    {
        $strJsonFileContents = file_get_contents("corporateUsers/users.json");

        $data = json_decode($strJsonFileContents, true);

        return $data[$user] ?? null;
    }

    public static function getBasicInfo()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "codigoEstabelecimento" => Estabelecimento::getData('codigoEstabelecimento'),
            "tipoEstabelecimento" => Estabelecimento::getData('tipoEstabelecimento')
        ];

        return Router::call($jsonData, 'web/estabelecimentosadmin/basicinfo');
    }

    public static function statusPlanSafe2Pay()
    {
        $name = urlencode(Estabelecimento::getData('nomeEstabelecimento') . Estabelecimento::getData('codigoEstabelecimento'));
        return Router::callSafe2Pay('https://services.safe2pay.com.br/Recurrence/V1/Subscriptions?CustomerName=' . $name . '&Status=Ativa');
    }
}
