<?php

namespace App\Model;

use App\Http\Router;

class Lojas
{
    public static function listarParceiros($data)
    {
        $jsonData = [
            "cep" => $data["cep"],
            "bairro" => $data["bairro"],
            "cidade" => $data["cidade"],
            "estado" => $data["estado"],
            "numero" => $data["numero"],
            "pais" => $data["pais"] ?? "Brasil",
            "rua" => $data["rua"]
        ];

        return Router::call($jsonData, 'https://us-central1-rainha-hamburgueria.cloudfunctions.net/api/v1/web/estabelecimentos/listarParceiros', true);
    }

    public static function indicacao($data, $endereco)
    {
        $jsonData = [
            "nome" => substr($data['nome'], 0, 140),
            "endereco" => $endereco
        ];

        return Router::call($jsonData, 'https://us-central1-rainha-hamburgueria.cloudfunctions.net/api/v1/cardapiodigital/indicacao', true);
    }
}
