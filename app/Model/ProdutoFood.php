<?php

namespace App\Model;

use \App\Http\Router;
use App\Session\Admin\Estabelecimento;

class ProdutoFood
{
    public static function getProdutos()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "codigoEstabelecimento" => Estabelecimento::getData('codigoEstabelecimento'),
            "filtro" => Estabelecimento::getData('ordenacaoProdutos')
        ];

        return Router::call($jsonData, 'web/produtos/listarcardapiodigital');
    }

    public static function getProdutoByCod($cod)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "codigoEstabelecimento" => Estabelecimento::getData('codigoEstabelecimento'),
            "key" => $cod
        ];

        return Router::call($jsonData, 'web/produtos/listarprodutoadicional');
    }
}
