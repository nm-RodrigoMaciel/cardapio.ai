<?php

namespace App\Model;

use \App\Http\Router;
use App\Session\Admin\Cliente;
use App\Session\Admin\Estabelecimento;
use App\Session\Admin\Pedido;

class Address
{
    public static function getFrete($retirada = null)
    {
        if ($retirada == null)
            $retirada = Pedido::getData('retirada');

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "tipoEstabelecimento" => Estabelecimento::getData('tipoEstabelecimento'),
            "location" => [
                "cep" => Cliente::getData('cep'),
                "bairro" => Cliente::getData('bairro'),
                "cidade" => Cliente::getData('cidade'),
                "complemento" => Cliente::getData('complemento'),
                "estado" => Cliente::getData('estado'),
                "numero" => Cliente::getData('numero'),
                "pais" => "Brasil",
                "rua" => Cliente::getData('rua')
            ],
            "retirada" => $retirada
        ];

        return Router::call($jsonData, 'validacao/frete/');
    }

    public static function getFreteValue()
    {
        $frete = self::getFrete();
        if ($frete != null) {
            return $frete['items']['valorFrete'];
        }
    }

    public static function latLongAddress($data)
    {
        $jsonData = [
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude']
        ];

        return Router::call($jsonData, 'https://us-central1-rainha-hamburgueria.cloudfunctions.net/api/v1/cardapiodigital/enderecoLatitudeLongitude/', true);
    }

    public static function getFreteCep($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::getData('nomeEstabelecimento'),
            "tipoEstabelecimento" => Estabelecimento::getData('tipoEstabelecimento'),
            "location" => [
                "cep" => preg_replace('/\D/', '', $data['cep'])
            ],
            "retirada" => false
        ];

        return Router::call($jsonData, 'validacao/frete/');
    }
}
