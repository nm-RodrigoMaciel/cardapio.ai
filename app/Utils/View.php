<?php

namespace App\Utils;

use App\Session\Admin\Estabelecimento;

class View
{
    private static $vars = [];

    public static function init($vars = [])
    {
        foreach($vars as $key => $value){
            self::$vars[$key] = $value;
        }
    }

    private static function getContentView($view)
    {
        $file = __DIR__ . '/../../view/' . $view . '.html';
        return file_exists($file) ? file_get_contents($file) : '';
    }

    public static function render($view, $vars = [])
    {
        $contentView = self::getContentView($view);

        //Logo WhiteLabel
        $vars = array_merge($vars, [
            'logo' => Estabelecimento::getData('logo')
        ]);

        $vars = array_merge(self::$vars, $vars);

        $keys = array_keys($vars);
        $keys = array_map(function ($item) {
            return '{{' . $item . '}}';
        }, $keys);

        return str_replace($keys, array_values($vars), $contentView);
    }
}
