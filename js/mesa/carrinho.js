/*Menos*/
$(document).on('click', '.item-pedido .buttons-elegant div:nth-child(1)', function () {
    updateQuantity($(this).parent().attr('ref'), 0);
});

/*Mais*/
$(document).on('click', '.item-pedido .buttons-elegant div:nth-child(3)', function () {
    updateQuantity($(this).parent().attr('ref'), 1);
});

function updateQuantity(index, qtd) {
    $.ajax({
        type: 'POST',
        url: 'updateObject',
        cache: false,
        data: { index: index, qtd: qtd },
        dataType: 'json'
    }).done(function (data) {
        qtd = parseInt(data.quantity);

        if (qtd < 1)
            $('.itens .item-pedido[ref="' + index + '"]').remove();
        else {
            $('.item-pedido[ref="' + index + '"] #quantity-value').text(qtd);
            $('.item-pedido[ref="' + index + '"] .value-area span:nth-child(2)').text(data.value);
        }

        $('.cart .value span:nth-child(2)').text('R$ ' + data.total);

        $('.cart .item-pedido').length == 0 ? $('.cart-section .back-arrow').click() : '';
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar atualizar este produto.');
        location.reload();
    });
}

function verifyRequired() {
    var qtd = 0;
    $('.cart input[type="text"]').each(function (index) {
        if ($(this).val() == "")
            qtd++;
    });

    if (qtd > 0) {
        $('.bill .bill-confirm').prop("disabled", true);
        return false;
    }

    if ($('#fone').val().length < 14) {
        $('.bill .bill-confirm').prop("disabled", true);
        return false;
    }

    $('.bill .bill-confirm').prop("disabled", false);
    return true;
}

$(document).on('input', '.cart-section input[type=text]', function () {
    verifyRequired();
});

$(document).on('click', '.bill-confirm', function () {
    $('.bill .bill-confirm').prop("disabled", true);
    if (verifyRequired()) {
        $.ajax({
            type: 'POST',
            url: 'verifyFinishCart',
            cache: false,
            data: {
                name: $('.cart-area .name-user input').val(),
                phone: $('.cart-area .telefone-user input').val()
            },
            dataType: 'json'
        }).done(function (data) {
            if (data.append !== undefined)
                eval(data.append);
        }).fail(function (data) {
            alert('Tivemos problemas ao tentar registrar seu pedido.');
            location.reload();
        });
    }
});

function finishCart(num) {
    closeQrCamera();
    startLoading();

    $.ajax({
        type: 'POST',
        url: 'finishCart',
        cache: false,
        data: {
            nameUser: $('.cart-area .name-user input').val(),
            phone: $('.cart-area .telefone-user input').val(),
            mesa: num
        },
        dataType: 'json'
    }).done(function (data) {
        if (data.append !== undefined)
            eval(data.append);
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar registrar seu pedido.');
        location.reload();
    });
}
