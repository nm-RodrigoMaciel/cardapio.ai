/*Expandable Forecast*/

$(document).on('click', '.linktree-profile-time-minimized', function () {
    if ($('.linktree-profile-time').hasClass('opened'))
        $('.linktree-profile-time').removeClass('opened');
    else
        $('.linktree-profile-time').addClass('opened');

    $(this).next('.linktree-profile-time-maximized').slideToggle(250);
});

/*Share Linktree*/

function clipboard(text) {
    navigator.clipboard.writeText(text);
    $('.linktree-share').addClass('message');
    setTimeout(
        function () {
            $('.linktree-share').removeClass('message');
        }, 3000);
}

$(document).on('click', '.linktree-share', function () {
    clipboard($(this).attr('link'));
});

/*Cardapio Digital*/

$(document).on('click', '.pedir-online', function () {
    $('.linktree').addClass('closed');
    $('.product-area').show();
});

/*Simulate Shipping*/

$(document).on('click', '.simulate-shipping:not(.result):not(.default) .simulate-shipping-field>div>div button', function () {
    var cep = $('.simulate-shipping-field>div>div input[type="text"]').val();

    if (cep.length > 8) {
        $('.simulate-shipping-field>div p:nth-child(1)').show();
        $('.simulate-shipping-error').hide();
        $.ajax({
            type: 'POST',
            url: 'simulateShipping',
            cache: false,
            data: { cep: cep },
            dataType: 'json'
        }).done(function (data) {
            $('.simulate-shipping').addClass('result');
            $('.simulate-shipping-field>div p:nth-child(1)').hide();
            $('.simulate-shipping-field>div p:nth-child(2)').append(data.valorFrete);
            $('.simulate-shipping-address').text(data.endereco);
        }).fail(function (data) {
            alert('Tivemos problemas ao tentar processar o cep.');
        });
    } else
        $('.simulate-shipping-error').show();
});

/*Location*/

$(document).on('click', '.linktree-body .address-button', function () {
    getLocation();
});

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, options);
    } else {
        alert("Seu navegador não suporta este recurso.");
    }
}

var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};

function success(position) {
    $.ajax({
        type: 'POST',
        url: 'latLongAddress',
        cache: false,
        data: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        },
        dataType: 'json'
    }).done(function (data) {
        $('.simulate-shipping-field>div>div input[type="text"]').val(data.cep);
        $('.simulate-shipping-field>div>div button').click();
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar receber a localização.');
    });
};

function error(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
};

/*Change Cep*/

$(document).on('click', '.simulate-shipping.result .simulate-shipping-field>div>div button', function () {
    $('.simulate-shipping-field>div p:nth-child(1)').show();
    $('.simulate-shipping-field>div p:nth-child(2)').text('');
    $('.simulate-shipping.result').removeClass('result');
});

/*Open Cep Default*/

$(document).on('click', '.simulate-shipping.default .simulate-shipping-field>div>div button', function () {
    $('.simulate-shipping').removeClass('default');
});