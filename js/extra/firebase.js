var firebaseConfig = {
    apiKey: "AIzaSyCwWEMAFfvzLP8zgUp-Ny6lglFUBvCdKoY",
    authDomain: "notificacoes-cfee3.firebaseapp.com",
    databaseURL: 'https://notificacoes-cfee3.firebaseio.com',
    projectId: "notificacoes-cfee3",
    storageBucket: "notificacoes-cfee3.appspot.com",
    messagingSenderId: "537274876489",
    appId: "1:537274876489:web:d9e461e8ba003264793598",
    measurementId: "G-7Y278YV8S6"
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

function notifyMe() {
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    }
    else if (Notification.permission === "granted") {
        intitalizeFireBaseMessaging();
    }
    else if (Notification.permission !== "denied") {
        Notification.requestPermission().then(function (permission) {
            if (permission === "granted") {
                intitalizeFireBaseMessaging();
            }
        });
    }
}

function intitalizeFireBaseMessaging() {
    messaging
        .requestPermission()
        .then(function () {
            console.log("Notification Permission");
            return messaging.getToken();
        })
        .then(function (token) {
            $.ajax({
                type: 'POST',
                url: 'setToken',
                cache: false,
                data: { token: token },
                dataType: 'text'
            });
        })
        .catch(function (reason) {
            console.log(reason);
        });
}

messaging.onMessage(function (payload) {
    console.log(payload);
    const notificationOption = {
        body: payload.notification.body,
        icon: payload.notification.icon
    };

    if (Notification.permission === "granted") {
        try {
            var notification = new Notification(payload.notification.title, notificationOption);

            notification.onclick = function (ev) {
                ev.preventDefault();
                window.open(payload.notification.click_action, '_blank');
                notification.close();
            }
        } catch (e) { }
    }
});
messaging.onTokenRefresh(function () {
    messaging.getToken()
        .then(function (newtoken) {
            $.ajax({
                type: 'POST',
                url: 'setToken',
                cache: false,
                data: { token: newtoken },
                dataType: 'text'
            });
        })
        .catch(function (reason) {
            console.log(reason);
        })
})

notifyMe();