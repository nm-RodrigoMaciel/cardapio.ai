/*Address*/

$(document).on('input', '.address-area-body input[type="text"]', function () {
    var disabled = false;
    $('.address-area-body input[type="text"][required]').each(function () {
        if ($(this).val() == "")
            disabled = true;
    });
    $('.address-area-button').prop('disabled', disabled);
});

$(document).on('click', '.address-area-button', function () {
    $(window).scrollTop(0);
    $('.lojas:not(.lojas-skeleton)').remove();
    $('.address-area').hide();
    $('.lojas-skeleton').show();
    var cliente = {
        cep: $('#a_CEP').val(),
        rua: $('#a_rua').val(),
        numero: $('#a_numero').val(),
        bairro: $('#a_bairro').val(),
        cidade: $('#a_cidade').val(),
        estado: $('#a_estado').val(),
        complemento: $('#a_complemento').val()
    };
    $.ajax({
        type: 'POST',
        url: 'getStores',
        cache: false,
        data: { cliente: JSON.stringify(cliente) },
        dataType: 'text'
    }).done(function (data) {
        $('.lojas-skeleton').hide();
        $('body').append(data);
        $('.lojas:not(.lojas-skeleton)').show();
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar procurar por estabelecimentos.');
        $('.address-area').block();
        $('.lojas-skeleton').hide();
    });
});

$(document).on('click', '.addressSelect', function () {
    $('.address-area').show();
    $('.lojas').hide();
});

$(document).on('click', '.suggestionSuccess button:nth-child(5)', function () {
    $('.suggestionBackground').click();
    $('.addressSelect').click();
});

/*Contais Function*/

jQuery.expr[':'].contains = function (a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

/*Select Categories*/

$(document).on('click', '.categories', function () {
    $has = $(this).hasClass('selected');
    $('.categories').removeClass('selected');
    if ($has) {
        $('.store').css('display', 'flex');
        $(this).removeClass('selected');
    }
    else {
        $('.store').hide();
        $(this).addClass('selected');
        $(".storeType:contains('" + $('p', this).text() + "')").parents('.store').css('display', 'flex');
    }
});

/*Lojas Rodape Button*/

$(document).on('click', '.lojasRodape button, .emptyList button', function () {
    $('.suggestionBackground').hide().fadeIn(150);
    $('.suggestionModal').hide().slideDown(300);
});

$(document).on('click', '.suggestionBackground', function () {
    $(this, '.suggestionBackground').fadeOut(150, function () {
        $(this).hide();
    });
    $('.suggestionModal').slideUp(150, function () {
        $(this).hide();
    });
    $('.suggestionSuccess').hide();
    $('.suggestionStart').hide().fadeIn(300);
    $('.suggestionStart input[type="text"]').val('');
});

$(document).on('input', '.suggestionStart input[type="text"]', function () {
    var disabled = !($(this).val() != '');
    $('.suggestionStart button').prop('disabled', disabled);
});

$(document).on('click', '.suggestionStart button', function () {
    $(this).prop('disabled', true);
    var nome = $('.suggestionStart input[type="text"]').val();
    if (nome != '') {
        $.ajax({
            type: 'POST',
            url: 'indicacao',
            cache: false,
            data: {
                nome: nome
            },
            dataType: 'text'
        }).done(function (data) {
            $('.suggestionStart button').prop('disabled', true);
            $('.suggestionStart input[type="text"]').val('');
            $('.suggestionStart').hide();
            $('.suggestionSuccess').hide().fadeIn(150);
        }).fail(function (data) {
            alert('Tivemos problemas ao tentar registrar a indicação.');
        });
    } else
        alert('Digite o nome do estabelecimento');
});

$(document).on('click', '.suggestionSuccess button:nth-child(4)', function () {
    $('.suggestionSuccess').hide();
    $('.suggestionStart').hide().fadeIn(150);
});

$(document).on('input', '.searchBar input[type="text"]', function () {
    $('.store, .emptyList, .categoriesChips').hide();
    $('.storesTitle, .lojasRodape').show();
    if ($(".storeTitle:contains('" + $(this).val() + "')").length < 1) {
        $('.storesTitle, .lojasRodape').hide();
        $('.addressSelect').show();
        $('.emptyList').hide().fadeIn(150);
    } else {
        $(".addressSelect, .categoriesChips").slideUp(150, function () {
            $(this).hide();
        });
        $(".storeTitle:contains('" + $(this).val() + "')").parents('.store').css('display', 'flex');
    }

    if ($(this).val() == '') {
        $(".addressSelect").show();
        $(".categoriesChips").css('display', 'flex');
        $(".store").css('display', 'flex');
    }
});

/*Location*/

$(document).on('click', '.address-area .address-button', function () {
    getLocation();
});

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, options);
    } else {
        alert("Seu navegador não suporta este recurso.");
    }
}

var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};

function success(position) {
    $.ajax({
        type: 'POST',
        url: 'latLongAddress',
        cache: false,
        data: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        },
        dataType: 'json'
    }).done(function (data) {
        $('#a_CEP').val(data.cep);
        $('#a_rua').val(data.rua);
        $('#a_numero').val(data.numero);
        $('#a_bairro').val(data.bairro);
        $('#a_cidade').val(data.cidade);
        $('#a_estado').val(data.estado_reduzido);
        $('.address-area-button').prop('disabled', false);
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar receber a localização.');
    });
};

function error(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
};