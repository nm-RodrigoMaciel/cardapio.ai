$(document).ready(function () {

    /*---------- CONTAINS FUNCTION ----------*/

    jQuery.expr[':'].contains = function (a, i, m) {
        return jQuery(a).text().toUpperCase()
            .indexOf(m[3].toUpperCase()) >= 0;
    };

    verifyQuantity();

    /*---------- CLICK ON PRODUCT ----------*/

    $(document).on('click', '.product-detais', function () {
        startLoading();
        $('.section, .bottom-bar, .linktree').hide();
        $('.product-section').show();

        $.ajax({
            type: 'POST',
            url: 'getProductByCod',
            cache: false,
            data: { cod: $('.product-title #cod', this).val() },
            dataType: 'text'
        }).done(function (data) {
            $('.product-section').append(data);
            $('body').data('scrollHome', document.documentElement.scrollTop);
        }).fail(function (data) {
            $.ajax({
                type: 'POST',
                url: 'seeLog',
                cache: false,
                data: { title: "Erro ao abrir um produto.", obj: JSON.stringify(data) },
                dataType: 'json'
            });
            startLoading(false);
            $('.product-section').hide();
            $('.product-area, .bottom-bar, .linktree').show();
        });
    });

    /*---------- FLOATING BUTTON ----------*/

    $(document).on('click', '.floating-button', function () {
        startLoading();
        $('.product-area').hide();
        $('.bottom-bar').hide();
        $('.linktree').hide();
        $('.cart-section').show();

        $.ajax({
            type: 'POST',
            url: 'getCart',
            cache: false,
            data: {},
            dataType: 'text'
        }).done(function (data) {
            startLoading(false);
            if (data == '' || data == '401') {
                $('.floating-button').addClass('hide');
                $('.cart-section').hide();
                $('.cart-section *').remove();
                goTo(0);
                showAlert("Escolha um produto primeiro.", "");
            } else if (data == '402') {
                $('.floating-button').addClass('hide');
                $('.cart-section').hide();
                $('.cart-section *').remove();
                goTo(0);
                showAlert("O estabelecimento está fechado.", "");
            } else {
                $('.cart-section').append(data);
                $('body').data('scrollHome', document.documentElement.scrollTop);
            }
        }).fail(function (data) {
            $.ajax({
                type: 'POST',
                url: 'seeLog',
                cache: false,
                data: { title: "Erro ao abrir o carrinho", obj: JSON.stringify(data) },
                dataType: 'json'
            });
            startLoading(false);
            $('.cart-section').hide();
            $('.product-area').show();
            $('.bottom-bar').show();
            $('.linktree').show();
        });
    });

    /*---------- SLIDER CATEGORY ----------*/

    $(document).on('click', '.category-option', function () {
        $('.category-option').removeClass('selected');
        $(this).addClass('selected');
        var ref = $(this).attr('ref');
        var position = $('#' + ref).offset().top;
        $(document).scrollTop(position - 125)
    });

    var lastCategory;
    var category;
    $(window).scroll(function () {
        if ($(window).scrollTop() + 100 + $(window).height() >= $(document).height()) {
            category = $(".title-category").last().attr('id');

            $('.category-option').removeClass('selected');
            $('.category-option[ref="' + category + '"]').addClass('selected');

            lastCategory = category;
        } else {
            if ($(document).scrollTop() < 450) {
                $('.product-area .search-area').css('opacity', '1');
                $('.product-area .default-area').css('opacity', '0');
            } else {
                $('.product-area .search-area').css('opacity', '0');
                $('.product-area .default-area').css('opacity', '1');
            }

            var position = $(document).scrollTop();

            $(".title-category").each(function () {
                if (position + 125 < $(this).offset().top) {
                    return false;
                }
                category = $(this).attr('id');
            });

            if (category != lastCategory) {
                $('.category-option').removeClass('selected');
                $('.category-option[ref="' + category + '"]').addClass('selected');

                $('.header-category').scrollLeft($('.category-option[ref="' + category + '"]').offset().left + $('.header-category').scrollLeft());

                lastCategory = category;
            }
        }
    });

    /*---------- BOTTOM BAR ----------*/

    $(document).on('click', '.bottom-bar .option', function () {
        goTo($(this).index());
    });

    /*---------- SEARCH AREA ----------*/

    $(document).on('input', '.search-area .search input[type="text"]', function () {
        if ($(this).val() != '') {
            $('.search-area-first').hide();
            $('.search-area-second').show();
            $('.search-area-second input[type="text"]').val($(this).val());
            $('.search-area-second input[type="text"]').focus();
            $('.search-area-second input[type="text"]').trigger("keypress");
            $('.search-area-second .search-result span:nth-child(2)').text('"' + $(this).val() + '":');

            buscarProduto($(this).val());
        }
    });

    $(document).on('click', '.seemore-area', function () {
        goTo(1);
    });

    $(document).on('click', '.search-area-second .icon-search-close', function () {
        $('.search-area .search input[type="text"]').val('');
        $('.search-area-second').hide();
        $('.search-area-first').show();
    });

    $(document).on('click', '.search-area .category', function () {
        goTo(0);

        var ref = $(this).attr('ref');

        $('.category-option').removeClass('selected');
        $('.category-option[ref="' + ref + '"]').addClass('selected');
        var position = $('#' + ref).offset().top;
        $(document).scrollTop(position - 125)
    });

    /*---------- PRODUCT SECTION ----------*/

    $(document).on('click', '.product-section .back-arrow', function () {
        $('.product-section').hide();
        $('.product-section *').remove();
        $('.product-section').text('');

        goTo(0);

        startLoading(false);
    });

    $(document).on('click', '.remove-ingredients', function () {
        if ($('.product-section .ingredientes').hasClass('closed')) {
            $('.product-section .ingredientes').removeClass('closed');
        } else {
            $('.product-section .ingredientes').addClass('closed');
        }
        $('.product-section .remove-ingredients svg').toggle();
    });

    /*---------- CART SECTION ----------*/

    $(document).on('click', '.cart-section .back-arrow', function () {
        $('.cart-section').hide();
        $('.cart-section *').remove();

        goTo(0);

        startLoading(false);
    });

    /********** PEDIDO SECTION **********/

    $(document).on('click', '.bill-area .back-arrow', function () {
        goTo(0);
    });

    // MASCARA DE TELEFONE
    var MaskFone = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000 0000' : '(00) 0000 00009';
    },
        Options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(MaskFone.apply({}, arguments), options);
            }
        };
    $('.fone').mask(MaskFone, Options);

    var CardFone = function (val) {
        return '0000 0000 0000 0000';
    },
        Options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(CardFone.apply({}, arguments), options);
            }
        };
    $('#c_input').mask(CardFone, Options);

    var Validity = function (val) {
        return '00/00';
    },
        Options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(Validity.apply({}, arguments), options);
            }
        };
    $('.ce_input').mask(Validity, Options);

    var Cpf = function (val) {
        return '000.000.000-00';
    },
        Options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(Cpf.apply({}, arguments), options);
            }
        };
    $('.ccp_input').mask(Cpf, Options);

    var Cep = function (val) {
        return '00000-000';
    },
        Options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(Cep.apply({}, arguments), options);
            }
        };
    $('.cep_input').mask(Cep, Options);
});

/********** SECTION OPIONS **********/

function goTo(pos) {
    $('.section').hide();
    $('.linktree').hide();

    if (pos == 0) {
        $('.linktree').show();
        $('.product-area').show();
        $('.bottom-bar').show();
        verifyQuantity();
    } else if (pos == 1) {
        $('.search-area').show();
    } else if (pos == 2) {
        finalBill();
        $('.final-bill').show();
    }

    if ($('body').data('scrollHome') == 0) {
        $('.product-area .search-area').css('opacity', '1');
        $('.product-area .default-area').css('opacity', '0');
        $('.category-option').removeClass('selected');
    }

    $(document).scrollTop($('body').data('scrollHome'));

    $('.bottom-bar .selected svg').addClass('item-disabled');
    $('.bottom-bar .selected').removeClass('selected');

    $('.bottom-bar .option-' + pos).addClass('selected');
    $('.bottom-bar .option-' + pos + ' svg').removeClass('item-disabled');

    $('body').data('scrollHome', document.documentElement.scrollTop);
}

/*---------- OTHERS ----------*/

var i = 2
function startLoading(open = true, type = '') {
    if (type == 'finish') {
        if (open) {
            $('body').append("<div class='finish-bill gif'><div class='center-abs'><img src='/cardapio.ai/img/gif/passo-2.gif'><div class='d-flex'><p>Processando pedido</p><span>.</span><span>.</span><span>.</span></div>​</div></div>");
            i = 2;
            changeGif();
        } else
            $('.finish-bill.gif').remove();
    } else {
        if (open)
            $('body').append("<div id='loading-content' style='position: fixed; background: #f8f9fa; width: 100%; height: 100%; top: 0; left: 0; z-index: 1500;'><img class='center-rel' src='/cardapio.ai/img/gif/loading.gif'/></div>");
        else
            $('#loading-content').remove();
    }
}

function changeGif() {
    setTimeout(() => {
        $('.finish-bill img').attr('src', '/img/gif/passo-' + i + '.gif');

        var text;
        switch (i) {
            case 2:
                text = 'Processando pedido';
                break;
            case 3:
                text = 'Validando pagamento';
                break;
            case 4:
                text = 'Validando endereço';
                break;
            case 5:
                text = 'Finalizando';
                break;
        }

        $('.finish-bill p').text(text);

        i++;
        verifyGif();
    }, 2000);
}

function verifyGif() {
    if (i <= 5)
        changeGif();
}

/********** BILL SECTION FUNCTIONS **********/

function verifyQuantity() {
    $.ajax({
        type: 'POST',
        url: 'getQuantityObject',
        cache: false,
        data: {},
        dataType: 'text'
    }).done(function (data) {
        data = parseInt(data);

        if (data > 0) {
            $('.floating-button').removeClass('hide');
            $('.floating-button div span').text(data);
        } else
            $('.floating-button').addClass('hide');

    }).fail(function (data) {
        alert('Tivemos problemas ao tentar processar o carrinho.');
        console.log(data);
    });
}

/********** CUSTOM ALERT ***********/

function showAlert(title, msg) {
    $('.custom-alert').remove();

    $('<div class="alert alert-warning custom-alert"><strong>' + title + '</strong></br>' + msg + '</div>')
        .appendTo('body')
        .hide().fadeIn(300);

    setTimeout(
        function () {
            $('.custom-alert').fadeOut(300, function () { $(this).remove(); });
        }, 5000);
}

function showAlertFinalBill() {
    $('.final-bill .alert').show();

    setTimeout(
        function () {
            $('.final-bill .alert').fadeOut(300, function () {
                $(this).hide();
            });
        }, 3000);
}

/********** ALERT FIELDS **********/

function showAlertField() {
    var can = true;
    removeRequired();

    $(".checkout-field:visible[required]").each(function (index, field) {
        var type = $(field).attr('type');

        if (type != null) {
            can = callbacks[type]();
        }
        else if (
            $('input', field).val() == '' ||
            (
                $(field).attr('min') != null &&
                $('input', field).val().length < $(field).attr('min')
            )
        ) {
            can = false;
            var location = $(field).offset();
            var text = $(field).attr('message') ?? 'Parâmetro incorreto';

            $(field).addClass('error');
            $($(field).closest('.section')).append('<div class="checkout-error" style="top:' + (location.top + 48) + 'px; left:' + (location.left - 4) + 'px"> <svg class="not-wl" width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M5.49296 5.49296V2.48826H4.50704V5.49296H5.49296ZM5.49296 7.51174V6.50235H4.50704V7.51174H5.49296ZM1.4554 1.47887C2.44131 0.492958 3.62285 0 5 0C6.37715 0 7.55086 0.492958 8.52113 1.47887C9.50704 2.44914 10 3.62285 10 5C10 6.37715 9.50704 7.55869 8.52113 8.5446C7.55086 9.51487 6.37715 10 5 10C3.62285 10 2.44131 9.51487 1.4554 8.5446C0.485133 7.55869 0 6.37715 0 5C0 3.62285 0.485133 2.44914 1.4554 1.47887Z" fill="#B00020" /> </svg> <span class="checkout-error-text">' + text + '</span> </div>');
        }
    });

    return can;
}

function removeRequired() {
    $('.checkout-error').remove();
    $(".checkout-field:visible[required]").removeClass('error');
}

/********** CPF VERIFY **********/

var callbacks = {
    cpf: function () {
        var strCPF = $('.ccp_input').val();
        var verify = true;
        var soma = 0;
        var resto;

        strCPF = strCPF.replace(/\./g, '');
        strCPF = strCPF.replace(/-/g, '');

        for (i = 1; i <= 9; i++) soma = soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        resto = (soma * 10) % 11;

        if ((resto == 10) || (resto == 11)) resto = 0;
        if (resto != parseInt(strCPF.substring(9, 10))) verify = false;

        soma = 0;
        for (i = 1; i <= 10; i++) soma = soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        resto = (soma * 10) % 11;

        if ((resto == 10) || (resto == 11)) resto = 0;
        if (resto != parseInt(strCPF.substring(10, 11))) verify = false;

        if (!verify) {
            var field = $('.ccp_input').parent();
            var text = $(field).attr('message') ?? 'Parâmetro incorreto';

            var location = $(field).offset();
            $(field).addClass('error');
            $($(field).closest('.section')).append('<div class="checkout-error" style="top:' + (location.top + 48) + 'px; left:' + (location.left - 4) + 'px"> <svg class="not-wl" width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M5.49296 5.49296V2.48826H4.50704V5.49296H5.49296ZM5.49296 7.51174V6.50235H4.50704V7.51174H5.49296ZM1.4554 1.47887C2.44131 0.492958 3.62285 0 5 0C6.37715 0 7.55086 0.492958 8.52113 1.47887C9.50704 2.44914 10 3.62285 10 5C10 6.37715 9.50704 7.55869 8.52113 8.5446C7.55086 9.51487 6.37715 10 5 10C3.62285 10 2.44131 9.51487 1.4554 8.5446C0.485133 7.55869 0 6.37715 0 5C0 3.62285 0.485133 2.44914 1.4554 1.47887Z" fill="#B00020" /> </svg> <span class="checkout-error-text">' + text + '</span> </div>');
        }

        return verify;
    },
    fone: function () {
        var strFONE = $('.fone:visible').val();
        var verify = true;

        verify = strFONE.length > 13;

        if (!verify) {
            var field = $('.fone:visible').parent();
            var text = $(field).attr('message') ?? 'Parâmetro incorreto';

            var location = $(field).offset();
            $(field).addClass('error');
            $($(field).closest('.section')).append('<div class="checkout-error" style="top:' + (location.top + 48) + 'px; left:' + (location.left - 4) + 'px"> <svg class="not-wl" width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M5.49296 5.49296V2.48826H4.50704V5.49296H5.49296ZM5.49296 7.51174V6.50235H4.50704V7.51174H5.49296ZM1.4554 1.47887C2.44131 0.492958 3.62285 0 5 0C6.37715 0 7.55086 0.492958 8.52113 1.47887C9.50704 2.44914 10 3.62285 10 5C10 6.37715 9.50704 7.55869 8.52113 8.5446C7.55086 9.51487 6.37715 10 5 10C3.62285 10 2.44131 9.51487 1.4554 8.5446C0.485133 7.55869 0 6.37715 0 5C0 3.62285 0.485133 2.44914 1.4554 1.47887Z" fill="#B00020" /> </svg> <span class="checkout-error-text">' + text + '</span> </div>');
        }

        return verify;
    }
};

/********** FINAL BILL **********/

function finalBill() {
    $('.bill-searched').remove();
    startLoading();

    $.ajax({
        type: 'POST',
        url: 'finalBill',
        cache: false,
        data: {},
        dataType: 'text'
    }).done(function (data) {
        $('.cart-section').hide();
        $('.final-bill').append(data);
        startLoading(false);
        showAlertFinalBill();
    }).fail(function (data) {
        $.ajax({
            type: 'POST',
            url: 'seeLog',
            cache: false,
            data: { title: "Erro ao finalizar o pedido", obj: JSON.stringify(data) },
            dataType: 'json'
        });
    });
}

/********** VERIFY SALES **********/

function verifySales() {
    setInterval(function () {
        if ($('.option-2.selected').length > 0)
            $('.bottom-bar .option-2').click();
    }, 30000);
}

verifySales();

/********** VERIFY PLAN **********/

$.ajax({
    type: 'POST',
    url: 'plan',
    cache: false,
    data: {},
    dataType: 'text'
}).done(function (data) {
    eval(data);
}).fail(function (data) {
    console.log(data);
});