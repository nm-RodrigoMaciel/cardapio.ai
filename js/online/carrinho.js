/*ELEGANT BUTTON QUANTITY*/
function updateQuantity(index, qtd) {
    $.ajax({
        type: 'POST',
        url: 'updateObject',
        cache: false,
        data: { index: index, qtd: qtd, f: $('#frete_H').val() },
        dataType: 'json'
    }).done(function (data) {
        qtd = parseInt(data.quantity);
        if (qtd < 1)
            $('.itens .item-pedido[ref="' + index + '"]').remove();
        else {
            $('.item-pedido[ref="' + index + '"] #quantity-value').text(qtd);
            $('.item-pedido[ref="' + index + '"] .value-area span:nth-child(2)').text(data.value);
        }

        $('.cart-area .order-bill div:nth-child(1) span:nth-child(2)').text('R$ ' + data.subtotal);
        $('.cart-area .order-bill div:last-child span:nth-child(2)').text('R$ ' + data.total);
        $('.bottom-action button p').text('Ver conta • R$ ' + data.total);

        if ($('.payment-method div p:nth-child(2)').text() === 'Dinheiro')
            updateChange();

        $('.cart .item-pedido').length == 0 ? $('.cart-section .back-arrow').click() : '';
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar atualizar este produto.');
        location.reload();
    });
}

/*Menos*/
$(document).on('click', '.cart .item-pedido .buttons-elegant div:nth-child(1)', function () {
    updateQuantity($(this).parent().attr('ref'), 0);
});

/*Mais*/
$(document).on('click', '.cart .item-pedido .buttons-elegant div:nth-child(3)', function () {
    updateQuantity($(this).parent().attr('ref'), 1);
});

$(document).on('click', '.coupon-area-button', function () {
    var ref = $(this);

    if (!$(ref).hasClass('opened')) {
        $(ref).addClass('opened');
        $('span', ref).text('Cancelar');
        $('.coupon-field').css('display', 'flex').hide().fadeIn(200);
    } else {
        $.ajax({
            type: 'POST',
            url: 'cleanCoupom',
            cache: false,
            data: {},
            dataType: 'text'
        }).done(function (data) {
            $('.coupon-field input[type="text"]').val('');
            $('.coupon-message').remove();

            $(ref).removeClass('opened');
            $('span', ref).text('Inserir');
            $('.coupon-field').hide();
            $('.coupon-field').removeClass('success').removeClass('error');
        }).fail(function (data) {
            alert('Tivemos problemas ao tentar atualizar este produto.');
            location.reload();
        });
    }
});

/*CHANGE SCROLL TEXT BOTTOM ACTION*/
$(document).scroll(function () {
    if ($(".cart-section").is(":visible"))
        buttonText();
});

$(document).on('click', '.cart .forecast', function () {
    $('.cart-area').hide();
    $('.schedule-area').show();
    $(window).scrollTop(0);
    $('.datepicker').click();
});

/********** Schedule Area **********/
$(document).on('click', '.back-arrow-schedule', function () {
    $('.timepicker-close, .datepicker-cancel').click();
    $('.schedule-area').hide();
    $('.cart-area').show();
    $(window).scrollTop(0);
});

$(document).on('click', '.opening-hours', function () {
    if ($(this).hasClass('opened')) {
        $(this).removeClass('opened');
    } else {
        $(this).addClass('opened');
    }
});

/*Inicializando o DataPicker, tornando ele vizivel e difinindo o dia atual como selecionado*/
$(document).on('click', '.datepicker-cancel', function () {
    $('.schedule-area').hide();
    $('.cart-area').show();
    $(window).scrollTop(0);
});

$(document).on('click', '.datepicker-done', function () {
    $('.timepicker').click();
});

/*Inicializando o TimePicker, ele será mostrado assim que a data for selecionada*/
$(document).on('click', '.timepicker-close:nth-child(1)', function () {
    $('.datepicker').click();
});

$(document).on('click', '.timepicker-done', function () {
    var date = Date.parse($('.datepicker').val() + ' ' + $('.timepicker').val());
    $.ajax({
        type: 'POST',
        url: 'setSchedule',
        cache: false,
        data: { date: date },
        dataType: 'json'
    }).done(function (data) {
        if (data.items != null) {
            $('.cart-section .back-arrow').click();
            if ($('.floating-button').is(':visible'))
                $('.floating-button').click();
        } else {
            showAlert('O restaurante estará fechado.', '');
            $('.datepicker').click();
        }
    }).fail(function (data) {
        alert(data.responseText);
        $('.back-arrow-schedule').click();
        $('.forecast').click();
    });

});

/********** Address Area **********/

$(document).on('click', '.back-arrow-address', function () {
    removeRequired();
    $('.cart-section .back-arrow').click();
    if ($('.floating-button').is(':visible'))
        $('.floating-button').click();
});

$(document).on('click', '.address-area-button', function () {
    if (showAlertField()) {
        var refButton = $(this);
        $(refButton).prop("disabled", true);

        var cliente = {
            nome: $('#a_nome').val(),
            telefone: $('#fone').val(),
            cep: $('#a_CEP').val(),
            endereco: $('#a_rua').val(),
            numero: $('#a_numero').val(),
            bairro: $('#a_bairro').val(),
            cidade: $('#a_cidade').val(),
            estado: $('#a_estado').val(),
            complemento: $('#a_complemento').val()
        };
        $.ajax({
            type: 'POST',
            url: 'setAddress',
            cache: false,
            data: { cliente: JSON.stringify(cliente) },
            dataType: 'text'
        }).done(function (data) {
            $(refButton).prop("disabled", false);
            removeRequired();
            $('.cart-section .back-arrow').click();
            if ($('.floating-button').is(':visible'))
                $('.floating-button').click();
        }).fail(function (data) {
            $(refButton).prop("disabled", false);
            $(window).scrollTop(0);
            $('.address-area .alert-red').css('display', 'flex');
            setTimeout(
                function () {
                    $('.address-area .alert-red').hide();
                }, 3000);
        });
    }
});

$(document).on('click', '.gps-button', function () {
    var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
    };

    function success(pos) {
        var crd = pos.coords;
        $.ajax({
            type: 'POST',
            url: 'latLongAddress',
            cache: false,
            data: {
                latitude: crd.latitude,
                longitude: crd.longitude
            },
            dataType: 'json'
        }).done(function (data) {
            $('#a_CEP').val(data.cep);
            $('#a_rua').val(data.rua);
            $('#a_numero').val(data.numero);
            $('#a_bairro').val(data.bairro);
            $('#a_cidade').val(data.cidade);
            $('#a_estado').val(data.estado_reduzido);
        }).fail(function (data) {
            alert('Tivemos problemas ao tentar receber a localização.');
        });
    };

    function error(err) {
        console.warn('ERROR(' + err.code + '): ' + err.message);
    };

    navigator.geolocation.getCurrentPosition(success, error, options);
});

/********** Paymenton App Area **********/

$(document).on('click', '.back-arrow-paymentonapp', function () {
    removeRequired();
    $(this).parent().parent().hide();
    $('.payment-area').show();
    $(window).scrollTop(0);
});

$(document).on('click', '.paymentonapp-type div', function () {
    $('.paymentonapp-type div').removeClass('selected');
    $(this).addClass('selected');
});

$(document).on('click', '.paymentonapp-area .bottom-action-button', function () {
    if (showAlertField()) {
        $.ajax({
            type: 'POST',
            url: 'setPaymentMethod',
            cache: false,
            data: {
                val_1: $('.cn_input').val(),
                val_2: $('.cna_input').val(),
                val_3: $('.ce_input').val(),
                val_4: $('.cc_input').val(),
                val_5: $('.ccp_input').val()
            },
            dataType: 'text'
        }).done(function (data) {
            $('.cart-section *').remove();
            $('.cart-section').hide();
            goTo(0);
            if ($('.floating-button').is(':visible'))
                $('.floating-button').click();
        }).fail(function (data) {
            alert('Tivemos problemas ao tentar cadastrar o cartão de credito.');
            location.reload();
        });
    }
});

$(document).on('focus', '.paymentonapp-area-body input[type="text"]', function () {
    $('.paymentonapp-cards-accepted-inner').addClass('rotate');
    $('.paymentonapp-cards-accepted-inner').removeClass('info-card');
});

$(document).on('focus', '.paymentonapp-area-body .ce_input, .paymentonapp-area-body .cc_input ,.paymentonapp-area-body .ccp_input', function () {
    $('.paymentonapp-cards-accepted-inner').addClass('info-card');
});

$(document).on('input', '.paymentonapp-area-body .ce_input', function () {
    $('.preview-card-info div:nth-child(1) p:nth-child(2)').text($(this).val());
    if ($(this).val() != '')
        $('.preview-card-info div:nth-child(1) p:nth-child(2)').addClass('texted');
    else {
        $('.preview-card-info div:nth-child(1) p:nth-child(2)').removeClass('texted');
        $('.preview-card-info div:nth-child(1) p:nth-child(2)').text('00/00');
    }
});

$(document).on('input', '.paymentonapp-area-body .cc_input', function () {
    $('.preview-card-info div:nth-child(2) p:nth-child(2)').text($(this).val());
    if ($(this).val() != '')
        $('.preview-card-info div:nth-child(2) p:nth-child(2)').addClass('texted');
    else {
        $('.preview-card-info div:nth-child(2) p:nth-child(2)').removeClass('texted');
        $('.preview-card-info div:nth-child(2) p:nth-child(2)').text('000');
    }
});

$(document).on('input', '.paymentonapp-area-body .ccp_input', function () {
    $('.preview-card-info div:nth-child(3) p:nth-child(2)').text($(this).val());
    if ($(this).val() != '')
        $('.preview-card-info div:nth-child(3) p:nth-child(2)').addClass('texted');
    else {
        $('.preview-card-info div:nth-child(3) p:nth-child(2)').removeClass('texted');
        $('.preview-card-info div:nth-child(3) p:nth-child(2)').text('000.000.000-00');
    }
});

$(document).on('input', '.paymentonapp-area-body .cn_input', function () {
    $('.preview-card-number p').text($(this).val());
    if ($(this).val() == '')
        $('.preview-card-number p').text('0000 0000 0000 0000');
    else
        $('.preview-card-number img').attr('src', '/img/card-flags/' + getFlagCard($(this).val())['flag']);
});

$(document).on('input', '.paymentonapp-area-body .cna_input', function () {
    $('.preview-card-info .holder-card').text($(this).val());
    if ($(this).val() != '')
        $('.preview-card-info .holder-card').addClass('texted');
    else {
        $('.preview-card-info .holder-card').removeClass('texted');
        $('.preview-card-info .holder-card').text('Nome do titular');
    }
});

/********** Paymentondelivery Area **********/

$(document).on('click', '.back-arrow-paymentondelivery', function () {
    removeRequired();
    $(this).parent().parent().hide();
    $('.cart-area').show();
    $(window).scrollTop(0);
});

function calculateTotal() {
    var valorFinal = 0.0;
    var quantityProd = 0;
    $(".cart .pedido-produto").each(function (index) {
        var valor = parseFloat($('.value-area span:nth-child(2)', $(this).parent()).text().replace(',', '.'));
        valorFinal += valor;
        quantityProd++;
    });

    if (quantityProd == 0)
        $('.bill-area .back-arrow').click();
    $('.cart-area .order-bill div:last-child span:nth-child(2)').text("R$ " + valorFinal.toFixed(2).replace('.', ','));
}

$(document).on('click', '.paymentondelivery-area .paymentOptionRow:not(.cards)', function () {
    paymentSet($(this).attr('class'), $(this).attr('id'));
});

$(document).on('click', '.paymentOptionRow-cards div', function () {
    var ref = $(this).parents('.paymentOptionRow');
    paymentSet($(ref).attr('class'), $(ref).attr('id'), $(this).index());
});

function paymentSet(val_1, val_6, val_7) {
    $.ajax({
        type: 'POST',
        url: 'setPaymentMethod',
        cache: false,
        data: {
            val_1: val_1.split(' ')[0],
            val_6: val_6,
            val_7: val_7
        },
        dataType: 'text'
    }).done(function (data) {
        $('.cart-section *').remove();
        $('.cart-section').hide();
        goTo(0);
        if ($('.floating-button').is(':visible'))
            $('.floating-button').click();
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar cadastrar o cartão de credito.');
        location.reload();
    });
}

$(document).on('click', '.paymentOptionRow.cards .paymentOptionRow-title', function () {
    var ref = $(this).parent();

    $('.paymentondelivery-area .paymentOptionRow').removeClass('disabled');
    if ($(ref).hasClass('opened')) {
        $(ref).removeClass('opened');
    } else {
        $('.paymentOptionRow').removeClass('opened').removeClass('disabled');
        $(ref).addClass('opened');
        $('.paymentondelivery-area .paymentOptionRow:not(.opened)').addClass('disabled');
    }
});

/*BUTTON TEXT*/
function buttonText() {
    if (
        $(window).height() + $(window).scrollTop() >=
        $('.cart-area .order-bill div:last-child span:nth-child(2)').offset().top + 130
    )
        $('.bottom-action').removeClass('info');
    else
        $('.bottom-action').addClass('info');
}

/*OPEN AREAS*/
$(document).on('click', '[stage]', function () {
    if ($(this).attr('class') !== undefined &&
        $(this).attr('class').includes('bottom-action-button') &&
        $('.bottom-action').hasClass('info'))
        $(window).scrollTop($(document).height());
    else {
        var stage = $(this).attr('stage');
        startLoading(true, stage);

        $.ajax({
            type: 'POST',
            url: 'openArea',
            cache: false,
            data: { page: stage },
            dataType: 'json'
        }).done(function (data) {
            startLoading(false, stage);
            $(window).scrollTop(0);
            $('.' + data.area).show();
            if (data.append !== undefined)
                eval(data.append);
        }).fail(function (data) {
            alert('Tivemos problemas ao tentar abrir a página.');
            location.reload();
        });
    }
});

/*SEND WPP MESSAGE*/
function sendMessage(phone, message) {
    window.open("https://api.whatsapp.com/send?phone=+55" + phone + "&text=" + message, "_blank");
}

/*CARD VERIFICATIONS*/
var PREFIXES_ELO = [
    "401178", "401179", "438935", "457631", "457632", "431274", "451416", "457393",
    "504175", "506699", "506778", "509000", "509999",
    "627780", "636297", "636368", "650031", "650033", "650035", "650051", "650405", "650439", "650485", "650538", "650541", "650598", "650700", "650718", "650720", "650727", "650901", "650978", "651652", "651679", "655000", "655019", "655021", "655058"
];
var PREFIXES_AMERICAN_EXPRESS = ["34", "37"];
var PREFIXES_DISCOVER = ["6011", "622", "64", "65"];
var PREFIXES_JCB = ["35"];
var PREFIXES_DINERS_CLUB = ["300", "301", "302", "303", "304", "305", "309", "36", "38", "39"];
var PREFIXES_VISA = ["4"];
var PREFIXES_MASTERCARD = [
    "2221", "2222", "2223", "2224", "2225", "2226", "2227", "2228", "2229",
    "223", "224", "225", "226", "227", "228", "229",
    "23", "24", "25", "26",
    "270", "271", "2720",
    "50", "51", "52", "53", "54", "55"
];
var PREFIXES_HIPERCARD = ["38", "60"];
function getFlagCard(cardNumber) {
    if (cardNumber != '') {
        if (hasAnyPrefix(cardNumber, PREFIXES_ELO)) {
            flag = { "name": "elo", "flag": "elo.svg" };
        } else if (hasAnyPrefix(cardNumber, PREFIXES_AMERICAN_EXPRESS)) {
            flag = { "name": "amex", "flag": "amex.svg" };
        } else if (hasAnyPrefix(cardNumber, PREFIXES_DISCOVER)) {
            flag = { "name": "discover", "flag": "discover.svg" };
        } else if (hasAnyPrefix(cardNumber, PREFIXES_JCB)) {
            flag = { "name": "jcb", "flag": "jcb.svg" };
        } else if (hasAnyPrefix(cardNumber, PREFIXES_DINERS_CLUB)) {
            flag = { "name": "diners", "flag": "diners.svg" };
        } else if (hasAnyPrefix(cardNumber, PREFIXES_VISA)) {
            flag = { "name": "visa", "flag": "visa.svg" };
        } else if (hasAnyPrefix(cardNumber, PREFIXES_MASTERCARD)) {
            flag = { "name": "mastercard", "flag": "mastercard.svg" };
        } else if (hasAnyPrefix(cardNumber, PREFIXES_HIPERCARD)) {
            flag = { "name": "hipercard", "flag": "hipercard.svg" };
        } else {
            flag = { "name": "payWithCreditCard", "flag": "default.svg" };
        }
    }
    return flag;
}
function hasAnyPrefix(number, prefixes) {
    var exists = false;
    $.each(prefixes, function (index, value) {
        if (number.startsWith(value)) {
            exists = true;
        }
    });

    return exists;
}
/*Bottom Modal*/
function showBottomModal() {
    $('#bottom-modal-fade').hide().fadeIn(300);
    $('#bottom-modal').animate({
        height: 320
    }, 500);
    $('#bottom-modal-button').animate({
        bottom: 48
    }, 500);
}
function closeBottomModal() {
    $('#bottom-modal-fade').fadeOut(150);
    $('#bottom-modal-button').animate({
        bottom: -48
    }, 350);
    $('#bottom-modal').animate({
        height: 0
    }, 300, function () {
    });

}
function verifyModalBottom() {
    var can = false;
    var value = $('#bottom-modal-value').maskMoney('unmasked')[0];
    var change = $('#bottom-modal-change').maskMoney('unmasked')[0];
    $('#bottom-modal-change').removeClass('error');
    $('#bottom-modal-topbutton').removeClass('error');
    if (change == 0) {
        $('#bottom-modal-topbutton').removeClass('change');
        $('#bottom-modal-topbutton').text('Não preciso de troco');
    }
    else if (change - value < 0) {
        $('#bottom-modal-topbutton').addClass('change').addClass('error');
        $('#bottom-modal-change').addClass('error');
        $('#bottom-modal-topbutton').text('Insira um valor igual ou maior que R$ ' + $.number(value, 2));
    }
    else if (change - value > 0) {
        $('#bottom-modal-topbutton').addClass('change');
        $('#bottom-modal-topbutton').text('Você receberá R$ ' + $.number((change - value), 2) + ' de troco');
        can = true;
    }
    return can;
}
$(document).on('keyup', '#bottom-modal-change', function () {
    verifyModalBottom();
});

$(document).on('click', '#bottom-modal-topbutton:not(.change):not(.error)', function () {
    $.ajax({
        type: 'POST',
        url: 'setChange',
        cache: false,
        data: {
            val_8: '0'
        },
        dataType: 'json'
    }).done(function (data) {
        closeBottomModal();
        if (parseFloat(data.troco) == 0) {
            $(".payment-method-text").remove();
            $(".payment-method div p:nth-child(3)").text('. Troco não solicitado.');
        }
    }).fail(function (data) {
        alert('Tivemos problemas ao definir o troco.');
        location.reload();
    });

});

$(document).on('click', '#bottom-modal-button', function () {
    if (verifyModalBottom()) {
        var val_8 = $('#bottom-modal-change').maskMoney('unmasked')[0];
        $.ajax({
            type: 'POST',
            url: 'setChange',
            cache: false,
            data: {
                val_8: val_8
            },
            dataType: 'json'
        }).done(function (data) {
            closeBottomModal();
            if (parseFloat(data.troco) > 0) {
                $(".payment-method-text").remove();
                $(".payment-method div p:nth-child(3)").text('. Troco para R$ ' + $.number((data.value), 2));
                $(".payment-method").append('<p class="payment-method-text">Você receberá R$ ' + $.number((data.troco), 2) + ' de troco</p>');
            }
        }).fail(function (data) {
            alert('Tivemos problemas ao definir o troco.');
            location.reload();
        });

    } else {
        var value = $('#bottom-modal-value').maskMoney('unmasked')[0];
        $('#bottom-modal-topbutton').addClass('change').addClass('error');
        $('#bottom-modal-change').addClass('error');
        $('#bottom-modal-topbutton').text('Insira um valor igual ou maior que R$ ' + $.number(value, 2));
    }
});

function updateChange() {
    $.ajax({
        type: 'POST',
        url: 'updateChange',
        cache: false,
        dataType: 'text'
    }).done(function (data) {
        if (data !== '') {
            if (parseFloat(data) > 0) {
                $(".payment-method-text").remove();
                $(".payment-method").append('<p class="payment-method-text">Você receberá R$ ' + $.number((data), 2) + ' de troco</p>');
            } else if (parseFloat(data) == 0) {
                $(".payment-method-text").remove();
                $(".payment-method div p:nth-child(3)").text(', ' + $('.cart-area .order-bill div:last-child span:nth-child(2)').text());
            } else {
                $('.bottom-action-button').attr("stage", "payment");
                $('.bottom-action-button::after').css('content', 'Escolher forma de pagamento');
                $('.payment-method').remove();
            }
        }
    }).fail(function (data) {
        alert('Tivemos problemas ao atualizar o troco.');
        location.reload();
    });

}
/********** Coupon area **********/

$(document).on('click', '.coupon-field-button', function () {
    $.ajax({
        type: 'POST',
        url: 'verifyCoupon',
        cache: false,
        data: { cupom: $('.coupon-field input[type="text"]').val() },
        dataType: 'json'
    }).done(function (data) {
        $('.coupon-message').remove();
        $('.coupon-field').removeClass('success').removeClass('error');
        $('.coupon-field').addClass(data.class);

        $('.cart-area .order-bill div:nth-child(1) span:nth-child(2)').text('R$ ' + data.subTotal);
        $('.cart-area .order-bill div:last-child span:nth-child(2)').text('R$ ' + data.total);
        $('.bottom-action button p').text('Ver conta • R$ ' + data.total);

        $('.cart-area .order-bill .freteInfo span:nth-child(2)').removeClass('free-shipping');
        if (data.class == 'success' && parseFloat(data.frete) == 0)
            $('.cart-area .order-bill .freteInfo span:nth-child(2)').text('GRÁTIS!').addClass('free-shipping');
        else
            $('.cart-area .order-bill .freteInfo span:nth-child(2)').text('R$ ' + data.frete);
        $('.cart-area .order-bill .discount').remove();
        $('.cart-area .order-bill div:last-child').before(data.discount);
        $('.coupon-field').after(data.textMessage);

        if ($('.payment-method div p:nth-child(2)').text() === 'Dinheiro')
            updateChange();
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar processar este cupom');
        location.reload();
    });

});

$(document).on('input', '.coupon-field input[type="text"]', function () {
    $('.coupon-message').animate({
        opacity: 0
    }, 150, function () {
        $('.coupon-message').remove();
    });

    $('.coupon-field').removeClass('success').removeClass('error');
});

$(document).on('click', '.item-pedido .buttons-elegant div:nth-child(1), .item-pedido .buttons-elegant div:nth-child(3)', function () {
    if ($(".coupon-field input[type=text]").val() != "") {
        $(".coupon-field-button").click();
    }
});

/********** Withdraw Area **********/

$(document).on('click', '.back-arrow-withdraw', function () {
    $('.cart-section .back-arrow').click();
    if ($('.floating-button').is(':visible'))
        $('.floating-button').click();
});

$(document).on('click', '.withdraw-area-button', function () {
    $.ajax({
        type: 'POST',
        url: 'setWithdraw',
        cache: false,
        data: {
            nome: $('[name="withdraw_name"]').val(),
            telefone: $('[name="withdraw_phone"]').val()
        },
        dataType: 'text'
    }).done(function (data) {
        $('.cart-section .back-arrow').click();
        if ($('.floating-button').is(':visible'))
            $('.floating-button').click();
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar realizar a requisição.');
    });
});