/*Expandable*/

$(document).on('click', '.final-bill .expandable-buttom-open', function () {
    $(this).parent().parent().hide(250);
    $(this).parent().parent().next('.maximized-item').slideToggle(250);
});

$(document).on('click', '.final-bill .expandable-buttom-close', function () {
    $(this).parent().slideToggle(250);
    $(this).parent().prev('.minimized-item').show(250);
});

/*Expandable Forecast*/

$(document).on('click', '.forecast-minimized', function () {
    if ($(this).hasClass('opened'))
        $(this).removeClass('opened');
    else
        $(this).addClass('opened');

    $(this).next('.forecast-maximized').slideToggle(250);
});