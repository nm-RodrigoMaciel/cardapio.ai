function verifyAdditionalRequired() {
    var verify = true;

    $(".product-section .itens").each(function (index) {
        if ($('.title-choose input', this).attr('type') == 'radio') {
            verify = false;
            $("input[type=radio]", this).each(function (index) {
                if ($(this).is(':checked')) {
                    verify = true;
                    $('.product-section .confirm').addClass('confirm-enabled');
                }
            });
            if (!verify) {
                $('.product-section .confirm').removeClass('confirm-enabled');
                return false;
            }
        }
    });
    return verify;
}

$(document).on('click', '.product-section .confirm', function () {
    if (verifyAdditionalRequired()) {
        $.ajax({
            type: 'POST',
            url: 'setProductsObject',
            cache: false,
            data: { nota: $('.product-section .note textarea').val() },
            dataType: 'text'
        }).done(function (data) {

            $('.product-section *').remove();
            $('.product-section').hide();

            goTo(0);
        }).fail(function (data) {
            alert('Tivemos problemas ao tentar adicionar este produto.');
            location.reload();
        });
    } else {
        showAlert('Adicionais obrigatórios', 'Preencha todos os adicionais obrigatórios.');
    }
});

/*Buttons radio adicionais*/

$(document).on('change', '.chooses input[type=radio]', function () {
    var key = $(this).attr('id');

    $.ajax({
        type: 'POST',
        url: 'updateQtdAdicionaisRadio',
        cache: false,
        data: { key: key },
        dataType: 'text'
    }).done(function (data) {
        verifyAdditionalRequired();
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar definir o adicional obrigátorio.');
        location.reload();
    });
});

/*Buttons elegant adicionais*/

$(document).on('click', '.product-section .buttons-elegant-adicionais div:nth-child(1), .product-section .buttons-elegant-adicionais div:nth-child(3)', function () {
    var key = $(this).parent().attr('key');
    var parent = $(this).parent();
    var root = $(this).parent().parent().parent().parent();
    $.ajax({
        type: 'POST',
        url: 'updateQtdAdicionais',
        cache: false,
        data: { key: key, option: $(this).index() },
        dataType: 'json'
    }).done(function (data) {
        $('div:nth-child(2) span', parent).text(data.qtd);
        if (data.all != undefined && data.all != null)
            data.all ? $('.buttons-elegant-adicionais div:nth-child(3) svg', root).removeClass('item-disabled-all') : $('.buttons-elegant-adicionais div:nth-child(3) svg', root).addClass('item-disabled-all');
        data.plus ? $('div:nth-child(3) svg', parent).removeClass('item-disabled') : $('div:nth-child(3) svg', parent).addClass('item-disabled');
        data.less ? $('div:nth-child(1) svg', parent).removeClass('item-disabled') : $('div:nth-child(1) svg', parent).addClass('item-disabled');
        $('.product-section #product-value').text(data.final);
    }).fail(function (data) {
        alert(data);
    });
});

/*Buttons elegant ingredientes*/

$(document).on('click', '.product-section .buttons-elegant-ingredientes div:nth-child(1), .product-section .buttons-elegant-ingredientes div:nth-child(3)', function () {
    var key = $(this).parent().attr('key');
    var parent = $(this).parent();
    $.ajax({
        type: 'POST',
        url: 'updateQtdIngredientes',
        cache: false,
        data: { key: key, option: $(this).index() },
        dataType: 'json'
    }).done(function (data) {
        $('div:nth-child(2) span', parent).text(data.qtd);
        data.plus ? $('div:nth-child(3) svg', parent).removeClass('item-disabled') : $('div:nth-child(3) svg', parent).addClass('item-disabled');
        data.less ? $('div:nth-child(1) svg', parent).removeClass('item-disabled') : $('div:nth-child(1) svg', parent).addClass('item-disabled');
    }).fail(function (data) {
        alert(data);
    });
});

/*Buttons elegant bottom*/

$(document).on('click', '.product-section .buttons-elegant-bottom div:nth-child(1), .product-section .buttons-elegant-bottom div:nth-child(3)', function () {
    var parent = $(this).parent();
    $.ajax({
        type: 'POST',
        url: 'updateQtdProduto',
        cache: false,
        data: { option: $(this).index() },
        dataType: 'json'
    }).done(function (data) {
        $('div:nth-child(2) span', parent).text(data.qtd);
        data.plus ? $('div:nth-child(3) svg', parent).removeClass('item-disabled') : $('div:nth-child(3) svg', parent).addClass('item-disabled');
        data.less ? $('div:nth-child(1) svg', parent).removeClass('item-disabled') : $('div:nth-child(1) svg', parent).addClass('item-disabled');
        $('.product-section #product-value').text(data.final);
    }).fail(function (data) {
        alert(data);
    });
});